/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'ko',
    'Magento_Checkout/js/view/form/element/email',
    'Magento_Customer/js/model/customer',
    'Mageplaza_Osc/js/model/osc-data',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Customer/js/action/check-email-availability',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/checkout-data',
    'mage/url',
    'rjsResolver',
    'mage/validation'
], function ($, ko, Component, customer, oscData, additionalValidators, checkEmailAvailability, quote, checkoutData, urlBuilder, resolver) {
    'use strict';

    var cacheKey = 'form_register_chechbox',
        allowGuestCheckout = window.checkoutConfig.oscConfig.allowGuestCheckout,
        passwordMinLength = window.checkoutConfig.oscConfig.register.dataPasswordMinLength,
        passwordMinCharacter = window.checkoutConfig.oscConfig.register.dataPasswordMinCharacterSets,
        customerEmailElement = '.form-login #customer-email';

    if (!customer.isLoggedIn() && (oscData.getData(cacheKey) !== false || !allowGuestCheckout)) {
        oscData.setData(cacheKey, true);
    }

    return Component.extend({
        defaults: {
            template: 'Mageplaza_Osc/container/form/element/email',
            isLoginVisible: false,
            listens: {
                email: ''
            }
        },
        checkDelay: 0,
        dataPasswordMinLength: passwordMinLength,
        dataPasswordMinCharacterSets: passwordMinCharacter,

        initialize: function () {
            this._super();

            if (!!this.email()) {
                resolver(this.emailHasChanged.bind(this));
            }

            additionalValidators.registerValidator(this);
        },

        initObservable: function () {
            this._super()
                .observe({
                    isCheckboxRegisterVisible: allowGuestCheckout,
                    isRegisterVisible: oscData.getData(cacheKey)
                });

            this.isRegisterVisible.subscribe(function (newValue) {
                oscData.setData(cacheKey, newValue);
            });

            return this;
        },

        triggerLogin: function () {
            if($('.osc-authentication-wrapper a.action-auth-toggle').hasClass('osc-authentication-toggle')){
                $('.osc-authentication-toggle').trigger('click');
            }else{
                window.location.href = urlBuilder.build("customer/account/login");
            }
        },

        /**
         * Callback on changing email property
         */
        emailHasChanged: function () {
            var self = this,
                loginFormSelector = 'form[data-role=email-with-possible-login]';

            clearTimeout(this.emailCheckTimeout);

            if (!customer.isLoggedIn()) {
                var loginFormCheck = setInterval(function () {
                    if ($(loginFormSelector).length) {
                        clearInterval(loginFormCheck);

                        if (self.validateEmail()) {
                            quote.guestEmail = self.email();
                            checkoutData.setValidatedEmailValue(self.email());
                        }
                        self.emailCheckTimeout = setTimeout(function () {
                            if (self.validateEmail()) {
                                self.checkEmailAvailability();
                                self.validateEmail(false);
                            } else {
                                self.isPasswordVisible(false);
                            }
                        }, self.checkDelay);

                        checkoutData.setInputFieldEmailValue(self.email());
                    }
                }, 200);
            }
        },

        /**
         * Check email existing.
         */
        checkEmailAvailability: function () {
            var self = this;

            this.validateRequest();
            this.isEmailCheckComplete = $.Deferred();
            this.isLoading(true);
            this.checkRequest = checkEmailAvailability(this.isEmailCheckComplete, this.email());

            $.when(this.isEmailCheckComplete).done(function () {
                self.isPasswordVisible(false);

                if (oscData.getData(cacheKey) !== false) {
                    self.isRegisterVisible(true);
                }
            }).fail(function () {
                self.isPasswordVisible(true);
                checkoutData.setCheckedEmailValue(self.email());

                $('.popup-authentication #login-email').val(self.email());
            }).always(function () {
                self.isLoading(false);
            });
        },

        validateEmail: function (focused) {
            var loginFormSelector = 'form[data-role=email-with-possible-login]',
                usernameSelector = loginFormSelector + ' input[name=username]',
                loginForm = $(loginFormSelector),
                validator;

            loginForm.validation();

            if (focused === false) {
                return !!$(usernameSelector).valid();
            }

            validator = loginForm.validate();

            return validator.check(usernameSelector);
        },

        validate: function (type) {
            if (customer.isLoggedIn() || !this.isRegisterVisible() || this.isPasswordVisible()) {
                oscData.setData('register', false);
                return true;
            }

            if (typeof type !== 'undefined') {
                var selector = $('#osc-' + type);

                selector.parents('form').validation();

                return !!selector.valid();
            }

            if (!customer.isLoggedIn() && this.isRegisterVisible() && !this.isPasswordVisible()) {
                this.checkEmailAvailability();
            }

            var passwordSelector = $('#osc-password');
            passwordSelector.parents('form').validation();

            var password = !!passwordSelector.valid();
            var confirm = !!$('#osc-password-confirmation').valid();

            var result = password && confirm;
            if (result) {
                oscData.setData('register', true);
                oscData.setData('password', passwordSelector.val());
            } else if (!password) {
                $('#osc-password').focus();
            } else if (!confirm) {
                $('#osc-password-confirmation').focus();
            }

            return result;
        },

        /** Move label element when input has value */
        hasValue: function(){
            if (window.checkoutConfig.oscConfig.isUsedMaterialDesign) {
                $(customerEmailElement).val() ? $(customerEmailElement).addClass('active') : $(customerEmailElement).removeClass('active');
            }
        }
    });
});
