define([
    'jquery'
], function ($) {
    "use strict";

    return function () {
        $.validator.addMethod(
            'validate-customer-password',
            function (v, elm) {
                var validator = this,
                    length = 0,
                    counter = 0;
                var passwordMinLength = $(elm).data('password-min-length');
                var passwordMinCharacterSets = $(elm).data('password-min-character-sets');
                var pass = $.trim(v);
                var result = pass.length >= passwordMinLength;
                if (result == false) {
                    /*eslint-disable max-len*/
                    validator.passwordErrorMessage = $.mage.__('Your desired text here. Minimum length of this field must be equal or greater than %1 symbols. Leading and trailing spaces will be ignored.').replace('%1', passwordMinLength);

                    /*eslint-enable max-len*/
                    return result;
                }
                if (pass.match(/\d+/)) {
                    counter ++;
                }
                if (pass.match(/[a-z]+/)) {
                    counter ++;
                }
                if (pass.match(/[A-Z]+/)) {
                    counter ++;
                }
                if (pass.match(/[^a-zA-Z0-9]+/)) {
                    counter ++;
                }
                if (counter < passwordMinCharacterSets) {
                    result = false;

                    /*eslint-disable max-len*/
                    validator.passwordErrorMessage = $.mage.__('Your desired text here. Minimum of different classes of characters in password is %1. Classes of characters: Lower Case, Upper Case, Digits, Special Characters.').replace('%1', passwordMinCharacterSets);

                    /*eslint-enable max-len*/
                }
                return result;
            }, function () {
                return this.passwordErrorMessage;
            }
        );
    }
});