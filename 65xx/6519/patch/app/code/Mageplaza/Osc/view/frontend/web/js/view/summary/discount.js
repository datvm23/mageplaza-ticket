/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'Magento_SalesRule/js/view/summary/discount',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/totals'
    ],
    function (Component, quote, total) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'Mageplaza_Osc/container/summary/discount'
            },
            getTitle: function() {
                var discount = total.getSegment('discount');

                if (!this.totals()) {
                    return null;
                }

                return discount.title;
            }
        });
    }
);
