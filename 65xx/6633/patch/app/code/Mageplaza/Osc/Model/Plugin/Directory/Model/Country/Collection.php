<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2017 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Osc\Model\Plugin\Directory\Model\Country;

/**
 * Plugin for processing incoming arguments of the method that leading to displaying additional empty dropdown.
 */
class Collection
{
    /**
     * Arguments processing.
     *
     * @param \Magento\Directory\Model\ResourceModel\Country\Collection $subject
     * @param string $emptyLabel
     *
     * @return bool|array
     */
    public function beforeToOptionArray(
        \Magento\Directory\Model\ResourceModel\Country\Collection $subject,
        $emptyLabel = ' '
    ) {
        return ($emptyLabel !== false) ? [''] : null;
    }
}