/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'ko',
        'Magento_Checkout/js/model/quote',
        'Magento_SalesRule/js/view/payment/discount',
        'Mageplaza_Osc/js/model/osc-loader/discount',
        'Mageplaza_Osc/js/model/osc-data',
        'Magento_Checkout/js/model/shipping-rate-service'
    ],
    function (ko, quote, Component, discountLoader, oscData, shippingRateService) {
        'use strict';

        var totals = quote.getTotals(),
            couponCode = ko.observable(null),
            cacheKey = 'gift_certificate_code';

        if (totals()) {
            couponCode(totals()['coupon_code']);
        }

        return Component.extend({
            defaults: {
                template: 'Mageplaza_Osc/container/review/discount'
            },
            isLoading: discountLoader.isLoading,
            giftCertificate: ko.observable(),

            /**
             * Applied flag
             */
            isApplied: ko.observable(couponCode() != null),

            initialize: function () {
                this._super();

                this.giftCertificate(oscData.getData(cacheKey));

                this.giftCertificate.subscribe(function (newValue) {
                    oscData.setData(cacheKey, newValue);
                });

                return this;
            },

            initObservable: function () {
                var self = this;

                this._super();

                quote.getTotals().subscribe(function (data) {
                    if (data.hasOwnProperty('coupon_code')) {
                        self.isApplied(true);
                    } else {
                        self.isApplied(false);
                        self.couponCode(null);
                    }
                }, this);

                return this;
            },

            /**
             * Coupon code application procedure
             */
            apply: function () {
                this._super();
                shippingRateService.estimateShippingMethod();
            },

            /**
             * Cancel using coupon
             */
            cancel: function () {
                this._super();
                shippingRateService.estimateShippingMethod();
            }
        });
    }
);
