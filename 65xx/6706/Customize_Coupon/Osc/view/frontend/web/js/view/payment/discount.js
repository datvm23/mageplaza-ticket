/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2017 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'ko',
        'Magento_SalesRule/js/view/payment/discount',
        'Mageplaza_Osc/js/model/osc-loader/discount',
        'Magento_Checkout/js/model/shipping-rate-service',
        'Mageplaza_Osc/js/model/coupon',
        'Magento_SalesRule/js/action/set-coupon-code',
        'Magento_SalesRule/js/action/cancel-coupon'
    ],
    function (ko, Component, discountLoader, shippingRateService, coupon, setCouponCodeAction, cancelCouponAction) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Mageplaza_Osc/container/review/discount'
            },
            isBlockLoading: discountLoader.isLoading,
            apply: function () {
                if (this.validate()) {
                    setCouponCodeAction(this.couponCode(), this.isApplied);
                }
                coupon.isLoadCoupon(true);
                shippingRateService.estimateShippingMethod();
            },

            /**
             * Cancel using coupon
             */
            cancel: function () {
                if (this.validate()) {
                    this.couponCode('');
                    cancelCouponAction(this.isApplied);
                }
                coupon.isLoadCoupon(true);
                shippingRateService.estimateShippingMethod();
            }
        });
    }
);
