/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'underscore',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/resource-url-manager',
        'Magento_Checkout/js/model/quote',
        'mage/storage',
        'Magento_Checkout/js/model/shipping-service',
        'Magento_Checkout/js/model/shipping-rate-registry',
        'Magento_Checkout/js/model/error-processor'
    ],
    function (_, checkoutData, resourceUrlManager, quote, storage, shippingService, rateRegistry, errorProcessor) {
        "use strict";
        return {
            getRates: function(address) {
                shippingService.isLoading(true);
                var cache = rateRegistry.get(address.getKey());
                if (cache) {
                    shippingService.setShippingRates(cache);
                    shippingService.isLoading(false);
                } else {
                    storage.post(
                        resourceUrlManager.getUrlForEstimationShippingMethodsByAddressId(),
                        JSON.stringify({
                            addressId: address.customerAddressId
                        }),
                        false
                    ).done(
                        function(result) {
                            var selectedShippingRate = checkoutData.getSelectedShippingRate(),
                                availableRate = result.length ? result[0] : false;

                            if (!availableRate && quote.shippingMethod()) {
                                availableRate = _.find(result, function (rate) {
                                    return rate.carrier_code == quote.shippingMethod().carrier_code &&
                                        rate.method_code == quote.shippingMethod().method_code;
                                });
                            }

                            if (!availableRate && selectedShippingRate) {
                                availableRate = _.find(result, function (rate) {
                                    return rate.carrier_code + '_' + rate.method_code === selectedShippingRate;
                                });
                            }

                            window.checkoutConfig.selectedShippingMethod = availableRate ? availableRate : null;

                            rateRegistry.set(address.getKey(), result);
                            shippingService.setShippingRates(result);
                        }

                    ).fail(
                        function(response) {
                            window.checkoutConfig.selectedShippingMethod = null;

                            shippingService.setShippingRates([]);
                            errorProcessor.process(response);
                        }
                    ).always(
                        function () {
                            shippingService.isLoading(false);
                        }
                    );
                }
            }
        };
    }
);
