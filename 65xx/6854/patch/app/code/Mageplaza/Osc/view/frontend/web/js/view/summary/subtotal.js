/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2017 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'underscore',
        'Magento_Tax/js/view/checkout/summary/subtotal'
    ],
    function (_, Component) {
        "use strict";

        var products = window.checkoutConfig.quoteItemData;

        return Component.extend({
            defaults: {
                template: 'Mageplaza_Osc/container/summary/subtotal'
            },

            // ISSUE 21: Get total price off
            getPriceOff: function () {
                var subtotal = 0,
                    productTotal = 0;
                    self = this;

                _.each(products, function (item) {
                    if (item && item.hasOwnProperty('product') &&
                        item.product.hasOwnProperty('price') && item.product.price &&
                        item.product.hasOwnProperty('special_price') && item.product.special_price) {
                        productTotal = (item.product.price - item.product.special_price);

                        _.each(self.totals().items, function (product) {
                            if (item.item_id == product.item_id) {
                                productTotal *= product.qty;
                            }
                        });

                        subtotal += productTotal;
                    }
                });

                return this.getFormattedPrice(subtotal);
            },
        });
    }
);
