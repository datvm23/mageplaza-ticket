<?php
/**
 * Copyright © 2017, Magma Consulting
 * See LICENSE for license details.
 */

namespace Magma\TaxCode\Block\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Checkout\Block\Checkout\AttributeMerger;
use Magento\Ui\Component\Form\AttributeMapper;
use Magento\Customer\Model\AttributeMetadataDataProvider;
use Magma\TaxCode\Setup\InstallData;
use Magma\TaxCode\Helper\Data as Helper;


class LayoutProcessor implements LayoutProcessorInterface
{
    /**
     * @var AttributeMetadataDataProvider
     */
    private $attributeMetadataDataProvider;

    /**
     * @var AttributeMapper
     */
    private $attributeMapper;

    protected $logger;
    /**
     * @var AttributeMerger
     */
    private $merger;

    /**
     * @var array
     */
    private $addressElements;

    private $helper;

    /**
     * LayoutProcessor constructor.
     * @param AttributeMetadataDataProvider $attributeMetadataDataProvider
     * @param AttributeMapper $attributeMapper
     * @param AttributeMerger $merger
     */
    public function __construct(
        AttributeMetadataDataProvider $attributeMetadataDataProvider,
        AttributeMapper $attributeMapper,
        AttributeMerger $merger,
        \Psr\Log\LoggerInterface $logger,
        Helper $helper
    ) {
        $this->attributeMetadataDataProvider = $attributeMetadataDataProvider;
        $this->attributeMapper = $attributeMapper;
        $this->merger = $merger;
        $this->logger = $logger;
        $this->helper = $helper;
    }

    /**
     * Process js Layout of block
     *
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        if ($this->helper->getIsModuleEnabled()){

            $paymentMethodRenders = $jsLayout['components']['checkout']['children']
            ['steps']['children']['billing-step']
            ['children']['payment']['children']
            ['payments-list']['children'];
            if (is_array($paymentMethodRenders)) {
                foreach ($paymentMethodRenders as $name => $renderer) {
                    if (isset($renderer['children']) && array_key_exists('form-fields', $renderer['children'])) {
                        $fields = $renderer['children']['form-fields']['children'];
                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']
                        ['children']['payment']['children']['payments-list']['children'][$name]['children']
                        ['form-fields']['children'] = $this->merger->merge(
                            $this->getAddressElements(),
                            'checkoutProvider',
                            $renderer['dataScopePrefix'] . '.custom_attributes',
                            $fields
                        );
                    }
                }
            }

            if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
                ['children']['shippingAddress']['children']['shipping-address-fieldset']['children']
            )) {
                $fields = $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
                ['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
                ['children']['shippingAddress']['children']['shipping-address-fieldset']['children'] = $this->merger->merge(
                    $this->getAddressElements(),
                    'checkoutProvider',
                    'shippingAddress.custom_attributes',
                    $fields
                );
            }

            $fields = &$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];
            foreach ($fields as $code => &$field) {
                $oriClasses = isset($field['config']['additionalClasses']) ? $field['config']['additionalClasses'] : '';
                switch ($code) {
                    case InstallData::IS_BUSINESS_ATTRIBUTE:
                        $field['sortOrder'] = $field['sortOrder'] ?: 0;
                        $field['config']['additionalClasses'] = $oriClasses . " col-mp mp-12";
                        break;
                    case 'country_id':
                        $field['sortOrder'] = $field['sortOrder'] ?: 10;
                        break;
                    case 'vat_id':
                        $field['sortOrder'] = $field['sortOrder'] ?: 11;
                        $field['config']['additionalClasses'] = $oriClasses . " col-mp mp-6";
                        break;
                    case InstallData::TAX_CODE_ATTRIBUTE:
                        $field['sortOrder'] = $field['sortOrder'] ?: 12;
                        $field['config']['additionalClasses'] = $oriClasses . " col-mp mp-6";
                        break;
                }
            }

            // $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            // ['children']['shippingAddress']['children']['shipping-address-fieldset']
            // ['children'][InstallData::IS_BUSINESS_ATTRIBUTE]['sortOrder'] = 0;

            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']
            ['children'][InstallData::IS_BUSINESS_ATTRIBUTE]['label'] = __('Is Business');

            $isBusinessOptions = $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']
            ['children'][InstallData::IS_BUSINESS_ATTRIBUTE]['options'];

            $translatedOptions = array();

            foreach ($isBusinessOptions as $option) {
                $option['value'] = __($option['value']);
                $option['label'] = __($option['label']);
                array_push($translatedOptions, $option);
            }

            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']
            ['children'][InstallData::IS_BUSINESS_ATTRIBUTE]['options'] = $translatedOptions;

            // $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            // ['children']['shippingAddress']['children']['shipping-address-fieldset']
            // ['children']['country_id']['sortOrder'] = 10;

            // $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            // ['children']['shippingAddress']['children']['shipping-address-fieldset']
            // ['children']['vat_id']['sortOrder'] = 11;

            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']
            ['children']['vat_id']['label'] = __('VAT number');

            // $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            // ['children']['shippingAddress']['children']['shipping-address-fieldset']
            // ['children'][InstallData::TAX_CODE_ATTRIBUTE]['sortOrder'] = 12;

            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']
            ['children'][InstallData::TAX_CODE_ATTRIBUTE]['label'] = __('Tax Code');
        }
        return $jsLayout;
    }

    /**
     * @return array
     */
    private function getAddressElements()
    {

        if (null === $this->addressElements) {
            $attributes = $this->attributeMetadataDataProvider->loadAttributesCollection(
                'customer_address',
                'customer_register_address'
            );
            $this->addressElements = [];

            foreach ($attributes as $attribute) {
                if (InstallData::TAX_CODE_ATTRIBUTE === $attribute->getAttributeCode()) {
                    //$this->logger->debug(print_r($attribute->getAllOptions(),true));
                    $this->addressElements[$attribute->getAttributeCode()] = $this->attributeMapper->map($attribute);
                }
                if (InstallData::IS_BUSINESS_ATTRIBUTE === $attribute->getAttributeCode()) {
                    $this->addressElements[$attribute->getAttributeCode()] = $this->attributeMapper->map($attribute);
                }
            }
        }


        //$this->logger->debug(print_r($this->addressElements['is_business'],true));

        return $this->addressElements;
    }
}
