<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_GiftCard
 * @copyright   Copyright (c) 2017 Mageplaza (https://www.mageplaza.com/)
 * @license     http://mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\GiftCard\Plugin\GiftCardAccount\Helper;

use Mageplaza\GiftCard\Helper\Checkout as CheckoutHelper;

/**
 * Class DataPlugin
 * @package Mageplaza\GiftCard\Plugin
 */
class DataPlugin
{
    /**
     * @type \Mageplaza\GiftCard\Helper\Checkout
     */
    protected $helper;

    /**
     * DataPlugin contructor.
     *
     * @param \Mageplaza\GiftCard\Helper\Checkout $checkoutHelper
     */
    public function __construct(CheckoutHelper $checkoutHelper)
    {
        $this->helper = $checkoutHelper;
    }

    /**
     * @param \Magento\GiftCardAccount\Helper\Data $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\DataObject $from
     * @return array|\Closure
     */
    public function aroundGetCards(\Magento\GiftCardAccount\Helper\Data $subject, \Closure $proceed, \Magento\Framework\DataObject $from)
    {
        if (!$this->helper->isEnabled()) {
            return $proceed($from);
        }

        return [];
    }
}
