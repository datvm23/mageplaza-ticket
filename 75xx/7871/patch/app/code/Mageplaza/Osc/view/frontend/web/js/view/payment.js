/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2017-2018 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/view/payment',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Mageplaza_Osc/js/model/checkout-data-resolver',
        'Mageplaza_Osc/js/model/payment-service',
        'Magento_Checkout/js/checkout-data',
        'mage/translate'
    ],
    function (ko,
              $,
              Component,
              quote,
              stepNavigator,
              additionalValidators,
              oscDataResolver,
              oscPaymentService,
              checkoutData) {
        'use strict';

        oscDataResolver.resolveDefaultPaymentMethod();

        return Component.extend({
            defaults: {
                template: 'Mageplaza_Osc/container/payment'
            },
            isLoading: oscPaymentService.isLoading,
            errorValidationMessage: ko.observable(false),

            initialize: function () {
                var self = this;

                this._super();

                stepNavigator.steps.removeAll();

                additionalValidators.registerValidator(this);

                quote.paymentMethod.subscribe(function () {
                    self.errorValidationMessage(false);
                });

                if (checkoutData.getSelectedPaymentMethod() == 'dibs_flexwin' && window.checkoutConfig.selectedPaymentMethod == 'dibs_flexwin') {
                    $.each(window.checkoutConfig.payment.dibsFlexwin.paytype, function (index, paytype) {
                        if (paytype.enabled) {
                            var paytypeInterval = setInterval(function () {
                                if ($('#' + paytype.id).length) {
                                    clearInterval(paytypeInterval);
                                    $('#' + paytype.id).trigger('click');
                                }
                            }, 200);
                            return false;
                        }
                    });
                }

                return this;
            },

            validate: function () {
                if (!quote.paymentMethod()) {
                    this.errorValidationMessage($.mage.__('Please specify a payment method.'));

                    $('html, body').scrollTop(
                        $('#checkout-step-payment').offset().top - $(window).height()/2
                    );

                    return false;
                }

                return true;
            }
        });
    }
);
