/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2017-2018 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'jquery',
        'ko',
        'Magento_Checkout/js/view/billing-address',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/checkout-data',
        'Mageplaza_Osc/js/model/osc-data',
        'Magento_Checkout/js/action/create-billing-address',
        'Magento_Checkout/js/action/create-shipping-address',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/action/set-billing-address',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Ui/js/model/messageList',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Mageplaza_Osc/js/model/address/auto-complete',
        'Mageplaza_Osc/js/model/compatible/amazon-pay',
        'uiRegistry',
        'mage/translate',
        'rjsResolver',
        'Mageplaza_Osc/js/model/billing-before-shipping',
        'Magento_Checkout/js/model/shipping-rate-service',
        'Mageplaza_Osc/js/model/shipping-rates-validator',
        'Magento_Customer/js/model/address-list'
    ],
    function ($,
              ko,
              Component,
              quote,
              checkoutData,
              oscData,
              createBillingAddress,
              createShippingAddress,
              selectBillingAddress,
              selectShippingAddress,
              customer,
              setBillingAddressAction,
              addressConverter,
              additionalValidators,
              globalMessageList,
              checkoutDataResolver,
              addressAutoComplete,
              amazonPay,
              registry,
              $t,
              resolver,
              billingBeforeShipping,
              shippingRateService,
              shippingRatesValidators,
              addressList) {
        'use strict';

        var observedElements = [],
            canShowBillingAddress = window.checkoutConfig.oscConfig.showBillingAddress,
            selectedShippingAddress = null,
            fieldSelectBillingElement = '.field-select-billing select[name=billing_address_id] option',
            reloadPage = true,
            hasSetDefaultAddress = false;

        return Component.extend({
            defaults: {
                template: ''
            },
            isCustomerLoggedIn: customer.isLoggedIn,
            isShowBillingBeforeShipping: window.checkoutConfig.oscConfig.showBillingBeforeShipping,
            isBillingSameShipping: !billingBeforeShipping.isBillingSameShipping(),
            isAmazonAccountLoggedIn: amazonPay.isAmazonAccountLoggedIn,
            quoteIsVirtual: quote.isVirtual(),

            canUseShippingAddress: ko.computed(function () {
                return !quote.isVirtual() && quote.shippingAddress() &&
                    quote.shippingAddress().canUseForBilling() && canShowBillingAddress;
            }),

            /**
             * @return {exports}
             */
            initialize: function () {
                var self = this;

                this._super();

                this.initFields();

                additionalValidators.registerValidator(this);

                registry.async('checkoutProvider')(function (checkoutProvider) {
                    var billingAddressData = checkoutData.getBillingAddressFromData();

                    if (billingAddressData) {
                        checkoutProvider.set(
                            'billingAddress',
                            $.extend({}, checkoutProvider.get('billingAddress'), billingAddressData)
                        );
                    }
                    checkoutProvider.on('billingAddress', function (billingAddressData) {
                        checkoutData.setBillingAddressFromData(billingAddressData);
                    });
                });

                if (this.isShowBillingBeforeShipping) {
                    quote.billingAddress.subscribe(function (newAddress) {
                        if (!billingBeforeShipping.isBillingSameShipping()) {
                            self.isAddressSameAsShipping(!billingBeforeShipping.isBillingSameShipping());
                            selectShippingAddress(newAddress);
                        }
                    });
                    this.isAddressFormVisible.subscribe(function () {
                        self.saveInAddressBook(true);
                    });
                    this.saveInAddressBook.subscribe(function () {
                        if (self.isAddressFormVisible()) self.saveNewBillingAddress();
                    });
                } else {
                    quote.shippingAddress.subscribe(function (newAddress) {
                        if (self.isAddressSameAsShipping()) {
                            selectBillingAddress(newAddress);
                        }
                    });
                }

                resolver(this.afterResolveDocument.bind(this));

                return this;
            },

            afterResolveDocument: function () {
                this.saveBillingAddress();

                addressAutoComplete.register('billing');
            },

            /**
             * set default address when page is reload
             */
            initDefaultAddress: function () {
                if (reloadPage && customer.isLoggedIn() && this.customerHasAddresses) {
                    var selectedBillingAddress = checkoutData.getSelectedBillingAddress(),
                        newCustomerBillingAddressData = checkoutData.getNewCustomerBillingAddress();
                    if (selectedBillingAddress) {
                        if (selectedBillingAddress == 'new-customer-address' && newCustomerBillingAddressData) {

                            $(fieldSelectBillingElement + ':last-child').prop('selected', true);
                            this.isAddressFormVisible(true);
                            this.setCustomerAddress('default-shipping', '');

                        } else {
                            this.setCustomerAddress('has-select-address', selectedBillingAddress);
                        }
                    } else if (window.customerData.default_billing != null) {
                        this.setCustomerAddress('default-billing', '');
                    }
                }
                reloadPage = false;
            },

            /**
             * @param condition
             * @param selectedValue
             */
            setCustomerAddress: function (condition, selectedValue) {
                var self = this;
                $.each(addressList(), function (key, address) {
                    if (condition == 'default-shipping') {
                        if (address.isDefaultShipping()) {
                            selectedShippingAddress = address;
                            return false;
                        }
                    }
                    if (condition == 'default-billing') {
                        if (address.isDefaultBilling()) {
                            $(fieldSelectBillingElement).eq(key).prop('selected', true);
                            self.selectAddress(address);
                            return false;
                        }
                    }

                    if (condition == 'has-select-address') {
                        if (selectedValue == address.getKey()) {
                            $(fieldSelectBillingElement).eq(key).prop('selected', true);
                            self.selectAddress(address);
                            return false;
                        }
                    }
                });
            },

            /**
             * Select Address
             * @param address
             */
            selectAddress: function (address) {
                if (!billingBeforeShipping.isBillingSameShipping()) {
                    selectedShippingAddress = address;
                    selectShippingAddress(address);
                    checkoutData.setSelectedShippingAddress(address.getKey());
                }
                selectBillingAddress(address);
                checkoutData.setSelectedBillingAddress(address.getKey());
            },


            /**
             * @return {Boolean}
             */
            useShippingAddress: function () {
                if (this.isShowBillingBeforeShipping) {
                    billingBeforeShipping.setBillingSameShipping();
                    if (!billingBeforeShipping.isBillingSameShipping()) {
                        if (this.selectedAddress() && !this.isAddressFormVisible()) {
                            selectShippingAddress(this.selectedAddress());
                            checkoutData.setSelectedShippingAddress(this.selectedAddress().getKey());
                        } else {
                            var addressFlat = addressConverter.formDataProviderToFlatData(this.collectObservedData(), 'billingAddress'),
                                address;
                            address = addressConverter.formAddressDataToQuoteAddress(addressFlat);
                            selectShippingAddress(address);
                            selectBillingAddress(address);
                        }
                        this.oscEstimateShippingMethod();

                    } else {
                        this.updateShippingAddress();
                    }

                } else {
                    if (this.isAddressSameAsShipping()) {
                        selectBillingAddress(quote.shippingAddress());
                        checkoutData.setSelectedBillingAddress(null);
                        if (window.checkoutConfig.reloadOnBillingAddress) {
                            setBillingAddressAction(globalMessageList);
                        }
                    } else {
                        this.updateAddress();
                    }
                }

                return true;
            },

            onAddressChange: function (address) {
                this._super(address);

                if (this.isShowBillingBeforeShipping) {
                    this.initDefaultAddress();
                    if (this.isAddressFormVisible()) {
                        this.saveNewBillingAddress();
                    } else {
                        if (hasSetDefaultAddress) {
                            this.selectAddress(this.selectedAddress());
                        }
                    }
                    this.oscEstimateShippingMethod();
                    hasSetDefaultAddress = true;
                } else {
                    if (!this.isAddressSameAsShipping() && canShowBillingAddress) {
                        this.updateAddress();
                    }
                }
            },

            /**
             * Save new Billing Address
             * @returns {*}
             */
            saveNewBillingAddress: function () {
                var addressData = this.source.get('billingAddress'),
                    newBillingAddress;
                if (customer.isLoggedIn() && !this.customerHasAddresses) {
                    this.saveInAddressBook(1);
                }
                addressData.save_in_address_book = this.saveInAddressBook() ? 1 : 0;
                newBillingAddress = createBillingAddress(addressData);

                // New address must be selected as a billing address
                selectBillingAddress(newBillingAddress);
                checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                checkoutData.setNewCustomerBillingAddress(addressData);
                return newBillingAddress;
            },

            /**
             * Estimate shipping method
             */
            oscEstimateShippingMethod: function () {
                shippingRateService.isAddressChange = true;
                clearTimeout(self.validateAddressTimeout);
                self.validateAddressTimeout = setTimeout(function () {
                    shippingRateService.estimateShippingMethod();
                }, 200);
            },

            /**
             * Update shipping address action
             */
            updateShippingAddress: function () {
                if (this.selectedAddress() && !this.isAddressFormVisible()) return;
                if (customer.isLoggedIn() && this.isAddressFormVisible() && this.customerHasAddresses) {
                    selectShippingAddress(selectedShippingAddress);
                } else {
                    var addressData = this.source.get('shippingAddress');
                    selectShippingAddress(addressConverter.formAddressDataToQuoteAddress(addressData));
                }
                this.oscEstimateShippingMethod();
            },

            /**
             * Update address action
             */
            updateAddress: function () {
                if (this.selectedAddress() && !this.isAddressFormVisible()) {
                    newBillingAddress = createBillingAddress(this.selectedAddress());
                    selectBillingAddress(newBillingAddress);
                    checkoutData.setSelectedBillingAddress(this.selectedAddress().getKey());
                } else {
                    var addressData = this.source.get('billingAddress'),
                        newBillingAddress;

                    if (customer.isLoggedIn() && !this.customerHasAddresses) {
                        this.saveInAddressBook(1);
                    }
                    addressData.save_in_address_book = this.saveInAddressBook() ? 1 : 0;
                    newBillingAddress = createBillingAddress(addressData);

                    // New address must be selected as a billing address
                    selectBillingAddress(newBillingAddress);
                    checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                    checkoutData.setNewCustomerBillingAddress(addressData);
                }

                if (window.checkoutConfig.reloadOnBillingAddress) {
                    setBillingAddressAction(globalMessageList);
                }
            },

            /**
             * Perform postponed binding for fieldset elements
             */
            initFields: function () {
                var self = this,
                    addressFields = window.checkoutConfig.oscConfig.addressFields,
                    fieldsetName = 'checkout.steps.shipping-step.billingAddress.billing-address-fieldset';

                $.each(addressFields, function (index, field) {
                    registry.async(fieldsetName + '.' + field)(self.bindHandler.bind(self));
                });

                return this;
            },

            bindHandler: function (element) {
                var self = this;

                if (element.component.indexOf('/group') !== -1) {
                    $.each(element.elems(), function (index, elem) {
                        self.bindHandler(elem);
                    });
                } else {
                    element.on('value', this.saveBillingAddress.bind(this, element.index));
                    observedElements.push(element);
                }
            },

            saveBillingAddress: function (fieldName) {
                /**
                 * when billing address before shipping address
                 */
                if (this.isShowBillingBeforeShipping) {
                    if (customer.isLoggedIn() && this.isAddressFormVisible()) {
                        var newBillingAddress = this.saveNewBillingAddress();
                        if (!billingBeforeShipping.isBillingSameShipping()) {
                            if (shippingRatesValidators.oscValidateAddressData(fieldName, newBillingAddress)) {
                                this.oscEstimateShippingMethod();
                            }
                        }
                    } else {
                        if (billingBeforeShipping.isBillingSameShipping()) {
                            var addressFlat = addressConverter.formDataProviderToFlatData(
                                this.collectObservedData(),
                                'billingAddress'
                            );

                            selectBillingAddress(addressConverter.formAddressDataToQuoteAddress(addressFlat));
                        }
                    }
                    return;
                }

                /**
                 * when shipping address before billing address
                 */
                if (!this.isAddressSameAsShipping()) {
                    if (!canShowBillingAddress && !this.quoteIsVirtual) {
                        selectBillingAddress(quote.shippingAddress());
                    } else if (this.isAddressFormVisible()) {
                        var addressFlat = addressConverter.formDataProviderToFlatData(
                            this.collectObservedData(),
                            'billingAddress'
                        ), newBillingAddress;

                        if (customer.isLoggedIn() && !this.customerHasAddresses) {
                            this.saveInAddressBook(1);
                        }
                        addressFlat.save_in_address_book = this.saveInAddressBook() ? 1 : 0;
                        newBillingAddress = createBillingAddress(addressFlat);

                        // New address must be selected as a billing address
                        selectBillingAddress(newBillingAddress);
                        checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                        checkoutData.setNewCustomerBillingAddress(addressFlat);

                        if (window.checkoutConfig.reloadOnBillingAddress && (fieldName == 'country_id')) {
                            setBillingAddressAction(globalMessageList);
                        }
                    }
                }
            },

            /**
             * Collect observed fields data to object
             *
             * @returns {*}
             */
            collectObservedData: function () {
                var observedValues = {};

                $.each(observedElements, function (index, field) {
                    observedValues[field.dataScope] = field.value();
                });

                return observedValues;
            },

            validate: function () {

                if (this.isAmazonAccountLoggedIn()) {
                    return true;
                }

                if (!this.isShowBillingBeforeShipping && this.isAddressSameAsShipping()) {
                    oscData.setData('same_as_shipping', true);
                    return true;
                }

                if (!this.isAddressFormVisible()) {
                    return true;
                }

                this.source.set('params.invalid', false);
                this.source.trigger('billingAddress.data.validate');

                if (this.source.get('billingAddress.custom_attributes')) {
                    this.source.trigger('billingAddress.custom_attributes.data.validate');
                }

                if (this.source.get('params.invalid')) {
                    var offsetHeight = $(window).height()/2,
                        errorMsgSelector = $('#billing-new-address-form .mage-error:first').closest('.field');

                    if (errorMsgSelector.length) {
                        if (errorMsgSelector.find('select').length) {
                            $('html, body').scrollTop(
                                errorMsgSelector.find('select').offset().top - offsetHeight
                            );
                            errorMsgSelector.find('select').focus();
                        } else if (errorMsgSelector.find('input').length) {
                            $('html, body').scrollTop(
                                errorMsgSelector.find('input').offset().top - offsetHeight
                            );
                            errorMsgSelector.find('input').focus();
                        }
                    }
                }

                if (!this.isShowBillingBeforeShipping) {
                    oscData.setData('same_as_shipping', false);
                }

                return !this.source.get('params.invalid');
            },
            getAddressTemplate: function () {
                return 'Mageplaza_Osc/container/address/billing-address';
            }
        });
    }
);
