<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_GiftCard
 * @copyright   Copyright (c) 2017 Mageplaza (https://www.mageplaza.com/)
 * @license     http://mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\GiftCard\Block\Product;

use Magento\Catalog\Block\Product\View\AbstractView;
use Magento\Catalog\Block\Product\Context;
use Magento\Config\Model\Config\Source\Locale\Timezone;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Locale\FormatInterface as LocaleFormat;
use Magento\Framework\Stdlib\ArrayUtils;
use Mageplaza\GiftCard\Helper\Data;
use Mageplaza\GiftCard\Model\Product\DeliveryMethods;
use Mageplaza\GiftCard\Model\Source\Status;
use Mageplaza\GiftCard\Model\TemplateFactory;

/**
 * Class View
 * @package Mageplaza\GiftCard\Block\Product
 */
class View extends AbstractView
{
    /**
     * @var array
     */
    protected $_templates = [];

    /**
     * @var \Mageplaza\GiftCard\Helper\Template
     */
    protected $templateHelper;

    /**
     * @var \Mageplaza\GiftCard\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Mageplaza\GiftCard\Model\TemplateFactory
     */
    protected $templateFactory;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Magento\Framework\Locale\FormatInterface
     */
    protected $localeFormat;

    /**
     * @var \Magento\Config\Model\Config\Source\Locale\Timezone
     */
    protected $timezoneSource;

    /**
     * @var \Mageplaza\GiftCard\Model\Product\DeliveryMethods
     */
    protected $deliveryMethods;

    /**
     * View constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Stdlib\ArrayUtils $arrayUtils
     * @param \Mageplaza\GiftCard\Model\TemplateFactory $templateFactory
     * @param \Mageplaza\GiftCard\Helper\Data $dataHelper
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Config\Model\Config\Source\Locale\Timezone $timezoneSource
     * @param \Mageplaza\GiftCard\Model\Product\DeliveryMethods $deliveryMethods
     * @param array $data
     */
    public function __construct(
        Context $context,
        ArrayUtils $arrayUtils,
        TemplateFactory $templateFactory,
        Data $dataHelper,
        PriceCurrencyInterface $priceCurrency,
        LocaleFormat $localeFormat,
        Timezone $timezoneSource,
        DeliveryMethods $deliveryMethods,
        array $data = []
    ) {

        parent::__construct($context, $arrayUtils, $data);

        $this->templateFactory = $templateFactory;
        $this->dataHelper      = $dataHelper;
        $this->templateHelper  = $dataHelper->getTemplateHelper();
        $this->priceCurrency   = $priceCurrency;
        $this->localeFormat    = $localeFormat;
        $this->timezoneSource  = $timezoneSource;
        $this->deliveryMethods = $deliveryMethods;

        $this->_templates = $this->initTemplates();
    }

    /**
     * @return int
     */
    public function isUseTemplate()
    {
        return sizeof($this->getTemplates());
    }

    /**
     * @return array
     */
    public function getTemplates()
    {
        return $this->_templates;
    }

    /**
     * @return array
     */
    public function getProductConfig()
    {
        return [
            'information' => $this->prepareInformation(),
            'template'    => $this->_templates
        ];
    }

    /**
     * @return array
     */
    public function prepareInformation()
    {
        $product       = $this->getProduct();
        $deliveryParam = $product->getConfigureMode()
            ? $product->getPreconfiguredValues()->getData()
            : [];

        $enableDeliveryDate = ($product->getGiftCardType() != DeliveryMethods::TYPE_PRINT) && $this->dataHelper->getProductConfig('enable_delivery_date');

        $information = [
            'productId'          => $product->getId(),
            'currencyRate'       => $this->priceCurrency->convert(1),
            'priceFormat'        => $this->localeFormat->getPriceFormat(),
            'amounts'            => $product->getGiftCardAmounts() ?: [],
            'delivery'           => $this->deliveryMethods->getDeliveryMethod($product->getGiftCardType(), $deliveryParam),
            'enableDeliveryDate' => $enableDeliveryDate,
            'timezone'           => [
                'enable'  => $enableDeliveryDate && $this->dataHelper->getProductConfig('enable_timezone'),
                'options' => $this->timezoneSource->toOptionArray(),
                'value'   => $this->dataHelper->getProductConfig('default_timezone') ?: $this->dataHelper->getConfigValue('general/locale/timezone')
            ],
            'deliveryDate'       => [
                'value'   => $this->dataHelper->getProductConfig('default_delivery_date')
            ],
            'fileUploadUrl'      => $this->_urlBuilder->getUrl('mpgiftcard/template/upload'),
            'messageMaxChar'     => $this->dataHelper->getMessageMaxChar()
        ];

        if ((boolean)$product->getAllowAmountRange()) {
            $minAmount = $product->getMinAmount();
            $minAmount = (!$minAmount || $minAmount < 0) ? 0 : $minAmount;

            $maxAmount = $product->getMaxAmount();
            $maxAmount = ($maxAmount && $maxAmount < $minAmount) ? $minAmount : $maxAmount;

            $priceRate = $product->getPriceRate() ?: 100;

            $information['openAmount'] = [
                'min'  => $minAmount,
                'max'  => $maxAmount,
                'rate' => $priceRate,
            ];
        }

        return $information;
    }

    /**
     * @return array
     */
    public function initTemplates()
    {
        $resultTemplates = [];
        $templateIds     = $this->getProduct()->getGiftProductTemplate();
        if ($templateIds) {
            $templates = $this->templateFactory->create()
                ->getCollection()
                ->addFieldToFilter('template_id', ['in' => explode(',', $templateIds)])
                ->addFieldToFilter('status', Status::STATUS_ACTIVE);
            foreach ($templates as $template) {
                $resultTemplates[$template->getId()] = $this->templateHelper->prepareTemplateData($template->getData());
            }
        }

        return $resultTemplates;
    }

    /**
     * @return array|mixed
     */
    public function getConfigureData()
    {
        $configureData = [];
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->getProduct();
        if ($product->getConfigureMode()) {
            $configureData = $product->getPreconfiguredValues()->getData();
        }

        if (($customer = $this->dataHelper->getCustomer()) && !isset($configureData['from'])) {
            $configureData['from'] = $customer->getName();
        }

        return $configureData;
    }

    /**
     * @inheritdoc
     */
    protected function _prepareLayout()
    {
        $this->pageConfig->addPageAsset('jquery/fileUploader/css/jquery.fileupload-ui.css');

        return parent::_prepareLayout();
    }
}
