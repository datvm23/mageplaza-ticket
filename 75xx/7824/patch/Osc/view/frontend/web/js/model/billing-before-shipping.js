/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
define(['ko', 'Mageplaza_Osc/js/model/osc-data'], function (ko, oscData) {
    'use strict';
    var isBillingSameShipping = oscData.getData('billing-same-shipping') ? oscData.getData('billing-same-shipping') : false;
    return {
        isBillingSameShipping: ko.observable(isBillingSameShipping),
        getBillingSameShipping: function () {
            return this.isBillingSameShipping();
        },
        setBillingSameShipping: function () {
            oscData.setData('billing-same-shipping', !this.isBillingSameShipping());
            return this.isBillingSameShipping(!this.isBillingSameShipping());
        }
    };
});