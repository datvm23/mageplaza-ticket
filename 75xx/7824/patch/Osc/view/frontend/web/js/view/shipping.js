/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'jquery',
        'ko',
        'underscore',
        'Magento_Checkout/js/view/shipping',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/action/set-shipping-information',
        'Mageplaza_Osc/js/action/payment-total-information',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/shipping-rate-service',
        'Magento_Checkout/js/model/shipping-service',
        'Mageplaza_Osc/js/model/checkout-data-resolver',
        'Mageplaza_Osc/js/model/address/auto-complete',
        'rjsResolver',
        'Mageplaza_Osc/js/model/billing-before-shipping',
        'Mageplaza_Osc/js/model/osc-data',
        'Mageplaza_Osc/js/model/compatible/amazon-pay'
    ],
    function ($,
              ko,
              _,
              Component,
              quote,
              customer,
              setShippingInformationAction,
              getPaymentTotalInformation,
              stepNavigator,
              additionalValidators,
              checkoutData,
              selectBillingAddress,
              selectShippingAddress,
              addressConverter,
              shippingRateService,
              shippingService,
              oscDataResolver,
              addressAutoComplete,
              resolver,
              billingBeforeShipping,
              oscData,
              amazonPay) {
        'use strict';

        oscDataResolver.resolveDefaultShippingMethod();

        /** Set shipping methods to collection */
        shippingService.setShippingRates(window.checkoutConfig.shippingMethods);

        return Component.extend({
            defaults: {
                template: 'Mageplaza_Osc/container/shipping'
            },
            currentMethod: null,
            isAmazonAccountLoggedIn: amazonPay.isAmazonAccountLoggedIn,
            isBillingSameShipping: billingBeforeShipping.isBillingSameShipping,
            isShowBillingBeforeShipping: window.checkoutConfig.oscConfig.showBillingBeforeShipping,
            initialize: function () {
                this._super();

                stepNavigator.steps.removeAll();

                //shippingRateService.estimateShippingMethod();
                additionalValidators.registerValidator(this);

                resolver(this.afterResolveDocument.bind(this));

                return this;
            },
            initObservable: function () {
                this._super();

                quote.shippingMethod.subscribe(function (oldValue) {
                    this.currentMethod = oldValue;
                }, this, 'beforeChange');

                quote.shippingMethod.subscribe(function (newValue) {
                    var type = quote.shippingAddress().getType(), rate = shippingService.getShippingRates()(), con = false;

                    if (newValue != null){
                        _.each(rate, function(value, index) {
                            if(newValue.method_code == value.method_code){
                                con = true;
                            }
                        });
                    }

                    if (newValue != null && con){
                        setShippingInformationAction();
                    }else if(oscData.change == false) {
                        getPaymentTotalInformation();
                    }else {
                        oscData.change = false;
                    }

                }, this);

                return this;
            },

            afterResolveDocument: function () {
                addressAutoComplete.register('shipping');

                shippingService.isLoading.subscribe(function (value) {
                    if (value) {
                        setTimeout(function () {
                            shippingService.isLoading(false);
                        }, 5000);
                    }
                });
            },

            validate: function () {
                if (this.isAmazonAccountLoggedIn()) {
                    return true;
                }

                if (quote.isVirtual()) {
                    return true;
                }
                var shippingMethodValidationResult = true;
                if (!quote.shippingMethod()) {
                    this.errorValidationMessage('Please specify a shipping method.');
                    shippingMethodValidationResult = false;
                }

                if (this.isShowBillingBeforeShipping) {
                    if (!billingBeforeShipping.isBillingSameShipping()) {
                        oscData.setData('billing-same-shipping', false);
                        return shippingMethodValidationResult;
                    }
                }


                var shippingAddressValidationResult = true,
                    loginFormSelector = 'form[data-role=email-with-possible-login]',
                    emailValidationResult = customer.isLoggedIn();

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }
                if (this.isShowBillingBeforeShipping && customer.isLoggedIn() && billingBeforeShipping.isBillingSameShipping() && this.isFormInline) {
                    var shippingAddress = quote.shippingAddress();
                    shippingAddress.save_in_address_book = 1;
                    selectShippingAddress(shippingAddress);
                }


                if (this.isFormInline) {
                    if (this.isShowBillingBeforeShipping) {
                        if (billingBeforeShipping.isBillingSameShipping()) {
                            this.source.set('params.invalid', false);
                            this.source.trigger('shippingAddress.data.validate');

                            if (this.source.get('shippingAddress.custom_attributes')) {
                                this.source.trigger('shippingAddress.custom_attributes.data.validate');
                            }

                            if (this.source.get('params.invalid')) {
                                shippingAddressValidationResult = false;
                            }

                            this.saveShippingAddress();
                        }
                    } else {
                        this.source.set('params.invalid', false);
                        this.source.trigger('shippingAddress.data.validate');

                        if (this.source.get('shippingAddress.custom_attributes')) {
                            this.source.trigger('shippingAddress.custom_attributes.data.validate');
                        }

                        if (this.source.get('params.invalid')) {
                            shippingAddressValidationResult = false;
                        }

                        this.saveShippingAddress();
                    }
                }

                if (!emailValidationResult) {
                    $(loginFormSelector + ' input[name=username]').focus();
                }

                return shippingMethodValidationResult && shippingAddressValidationResult && emailValidationResult;
            },
            saveShippingAddress: function () {
                var shippingAddress = quote.shippingAddress(),
                    addressData = addressConverter.formAddressDataToQuoteAddress(
                        this.source.get('shippingAddress')
                    );

                //Copy form data to quote shipping address object
                for (var field in addressData) {
                    if (addressData.hasOwnProperty(field) &&
                        shippingAddress.hasOwnProperty(field) &&
                        typeof addressData[field] != 'function' &&
                        _.isEqual(shippingAddress[field], addressData[field])
                    ) {
                        shippingAddress[field] = addressData[field];
                    } else if (typeof addressData[field] != 'function' && !_.isEqual(shippingAddress[field], addressData[field])) {
                        shippingAddress = addressData;
                        break;
                    }
                }

                if (customer.isLoggedIn()) {
                    shippingAddress.save_in_address_book = 1;
                }
                selectShippingAddress(shippingAddress);
            },

            saveNewAddress: function () {

                this.source.set('params.invalid', false);
                if (this.source.get('shippingAddress.custom_attributes')) {
                    this.source.trigger('shippingAddress.custom_attributes.data.validate');
                }

                if (!this.source.get('params.invalid')) {
                    this._super();
                }

                if (!this.source.get('params.invalid')) {
                    shippingRateService.isAddressChange = true;
                    shippingRateService.estimateShippingMethod();
                }
            },

            getAddressTemplate: function () {
                return 'Mageplaza_Osc/container/address/shipping-address';
            }
        });
    }
);
