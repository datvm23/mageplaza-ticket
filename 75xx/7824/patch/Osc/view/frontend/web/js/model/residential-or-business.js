/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */


define(['ko','uiRegistry'], function(ko,registry) {
    'use strict';
    var shippingAddressFieldsetPath = 'checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset',
        billingAddressFieldsetPath  = 'checkout.steps.shipping-step.billingAddress.billing-address-fieldset'

    var ADDRESS_TYPE_OPTION = {
        RESIDENTIAL: 'residential',
        BUSINESS: 'business'
    };
    return {
        addressType: ko.observable(ADDRESS_TYPE_OPTION.RESIDENTIAL),
        init:function(){
            registry.async()(this.asyncShippingAddressFieldset.bind(this));
        },
        checkShippingAddressType: function () {
            var companyShippingComponent = registry.get(shippingAddressFieldsetPath + '.company'),
                companyBillingComponent = registry.get(billingAddressFieldsetPath + '.company');
            if(companyShippingComponent || companyBillingComponent) {
                var isBusiness = this.addressType() === ADDRESS_TYPE_OPTION.BUSINESS;
                companyShippingComponent.visible(isBusiness);
                companyShippingComponent.value(
                    isBusiness ? registry.get(shippingAddressFieldsetPath + '.company').value() : ''
                );
                companyBillingComponent.visible(isBusiness);
                companyBillingComponent.value(
                    isBusiness ? registry.get(billingAddressFieldsetPath + '.company').value() : ''
                )
            }

        },

        asyncShippingAddressFieldset: function () {
            this.checkShippingAddressType();
            this.addressType.subscribe(function(newValue) {
                this.checkShippingAddressType();
            }, this);
        }
    };
});