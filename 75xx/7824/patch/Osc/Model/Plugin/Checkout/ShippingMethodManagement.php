<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
namespace Mageplaza\Osc\Model\Plugin\Checkout;

use Magento\Quote\Api\Data\EstimateAddressInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Api\Data\CartExtensionFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Quote\Model\Quote\AddressFactory;
use Magento\Quote\Model\ShippingAssignmentFactory;
use Magento\Quote\Model\ShippingFactory;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Tax\Controller\Adminhtml\Rate\Add;

/**
 * Class ShippingMethodManagement
 * @package Mageplaza\Osc\Model\Plugin\Checkout
 */
class ShippingMethodManagement
{
	/**
	 * @var \Magento\Quote\Api\Data\CartExtensionFactory
	 */
	private $cartExtensionFactory;

	/**
	 * @var \Magento\Quote\Model\ShippingFactory
	 */
	private $shippingFactory;

	/**
	 * @var \Magento\Quote\Model\ShippingAssignmentFactory
	 */
	protected $shippingAssignmentFactory;

	/**
	 * Quote repository.
	 *
	 * @var \Magento\Quote\Api\CartRepositoryInterface
	 */
	protected $quoteRepository;

	/**
	 * @var AddressInterface
	 */
	protected $addressInterface;

	protected $addressFactory;

	/**
	 * Customer Address repository
	 *
	 * @var \Magento\Customer\Api\AddressRepositoryInterface
	 */
	protected $addressRepository;

	/**
	 * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
	 * @param \Magento\Customer\Api\AddressRepositoryInterface $addressRepository
	 * @param \Magento\Quote\Api\Data\AddressInterface $addressInterface
	 * @param \Magento\Quote\Model\Quote\AddressFactory $addressFactory
	 */
	public function __construct(
		\Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
		\Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
		AddressInterface $addressInterface,
		AddressFactory $addressFactory
	)
	{

		$this->quoteRepository   = $quoteRepository;
		$this->addressRepository = $addressRepository;
		$this->addressInterface  = $addressInterface;
		$this->addressFactory    = $addressFactory;
	}

	/**
	 * @param \Magento\Quote\Model\ShippingMethodManagement $subject
	 * @param \Closure $proceed
	 * @param $cartId
	 * @param EstimateAddressInterface $address
	 * @return array
	 */
	public function aroundEstimateByAddress(
		\Magento\Quote\Model\ShippingMethodManagement $subject,
		\Closure $proceed,
		$cartId,
		EstimateAddressInterface $address
	)
	{
		$this->saveAddress($cartId, $address);

		return $proceed($cartId, $address);
	}

	/**
	 * @param \Magento\Quote\Model\ShippingMethodManagement $subject
	 * @param \Closure $proceed
	 * @param $cartId
	 * @param \Magento\Quote\Api\Data\AddressInterface $address
	 * @return mixed
	 */
	public function aroundEstimateByExtendedAddress(
		\Magento\Quote\Model\ShippingMethodManagement $subject,
		\Closure $proceed,
		$cartId,
		AddressInterface $address
	)
	{

		$this->saveAddress($cartId, $address);

		return $proceed($cartId, $address);
	}

	/**
	 * @param \Magento\Quote\Model\ShippingMethodManagement $subject
	 * @param \Closure $proceed
	 * @param $cartId
	 * @param $addressId
	 * @return mixed
	 */
	public function aroundEstimateByAddressId(
		\Magento\Quote\Model\ShippingMethodManagement $subject,
		\Closure $proceed,
		$cartId,
		$addressId
	)
	{

		$address = $this->addressRepository->getById($addressId);
		$this->saveAddress($cartId, $address);

		return $proceed($cartId, $addressId);
	}

	/**
	 * @param $cartId
	 * @param $address
	 * @return $this
	 */
	private function saveAddress($cartId, $address)
	{
		/** @var \Magento\Quote\Model\Quote $quote */
		$quote = $this->quoteRepository->getActive($cartId);

		if (!$quote->isVirtual()) {
			$addressData = [
				EstimateAddressInterface::KEY_COUNTRY_ID => $address->getCountryId(),
				EstimateAddressInterface::KEY_POSTCODE   => $address->getPostcode(),
				EstimateAddressInterface::KEY_REGION_ID  => $address->getRegionId(),
				EstimateAddressInterface::KEY_REGION     => $address->getRegion()
			];

			$addressInterface = $this->addressFactory->create()
				->setCountryId($address->getCountryId())
				->setPostcode($address->getPostcode())
				->setRegionId($address->getRegionId())
				->setRegion($address->getRegion());

			$shippingAddress = $quote->getShippingAddress();
			try {
				$quote = $this->prepareShippingAssignment($quote, $addressInterface);
				$this->validateQuote($quote);

				$shippingAddress->addData($addressData)
					->save();
				$quote->setShippingAddress($shippingAddress);
				$this->quoteRepository->save($quote);
			} catch (\Exception $e) {
				return $this;
			}
		}

		return $this;
	}

	/**
	 * Validate quote
	 *
	 * @param \Magento\Quote\Model\Quote $quote
	 * @throws InputException
	 * @throws NoSuchEntityException
	 * @return void
	 */
	protected function validateQuote(\Magento\Quote\Model\Quote $quote)
	{
		if (0 == $quote->getItemsCount()) {
			throw new InputException(__('Shipping method is not applicable for empty cart'));
		}
	}

	/**
	 * @param CartInterface $quote
	 * @param AddressInterface $address
	 * @return CartInterface
	 */
	private function prepareShippingAssignment(CartInterface $quote, AddressInterface $address)
	{
		$cartExtension = $quote->getExtensionAttributes();
		if ($cartExtension === null) {
			$cartExtension = $this->getCartExtensionFactory()->create();
		}

		$shippingAssignments = $cartExtension->getShippingAssignments();
		if (empty($shippingAssignments)) {
			$shippingAssignment = $this->getShippingAssignmentFactory()->create();
		} else {
			$shippingAssignment = $shippingAssignments[0];
		}

		$shipping = $shippingAssignment->getShipping();
		if ($shipping === null) {
			$shipping = $this->getShippingFactory()->create();
		}

		$shipping->setAddress($address);
		$shippingAssignment->setShipping($shipping);
		$cartExtension->setShippingAssignments([$shippingAssignment]);

		return $quote->setExtensionAttributes($cartExtension);
	}

	/**
	 * @return ShippingAssignmentFactory
	 */
	private function getShippingAssignmentFactory()
	{
		if (!$this->shippingAssignmentFactory) {
			$this->shippingAssignmentFactory = ObjectManager::getInstance()->get(ShippingAssignmentFactory::class);
		}

		return $this->shippingAssignmentFactory;
	}

	/**
	 * @return CartExtensionFactory
	 */
	private function getCartExtensionFactory()
	{
		if (!$this->cartExtensionFactory) {
			$this->cartExtensionFactory = ObjectManager::getInstance()->get(CartExtensionFactory::class);
		}

		return $this->cartExtensionFactory;
	}

	/**
	 * @return ShippingFactory
	 */
	private function getShippingFactory()
	{
		if (!$this->shippingFactory) {
			$this->shippingFactory = ObjectManager::getInstance()->get(ShippingFactory::class);
		}

		return $this->shippingFactory;
	}
}
