<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_OscCustomField
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
namespace Mageplaza\OscCustomField\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class QuoteSubmitBefore
 * @package Mageplaza\Osc\Observer
 */
class QuoteSubmitBefore implements ObserverInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession
    )
    {

        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        $quoteShippingAddress = $quote->getShippingAddress();
        $quoteBillingAddress = $quote->getBillingAddress();
        $orderShippingAddress = $order->getShippingAddress();
        $orderBillingAddress = $order->getBillingAddress();

        if (!$quote->isVirtual()
            && $orderShippingAddress
            && $quoteShippingAddress) {
            $orderShippingAddress->setData(
                'housenumber',
                $quoteShippingAddress->getData('housenumber')
            );
        }

        if ($orderBillingAddress
            && $quoteBillingAddress) {
            $orderBillingAddress->setData(
                'housenumber',
                $quoteBillingAddress->getData('housenumber')
            );
        }
    }
}
