<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_OscCustomField
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\OscCustomField\Helper;

/**
 * Class Config
 * @package Mageplaza\Osc\Helper
 */
class Config extends \Mageplaza\Osc\Helper\Data
{
    /**
     * @param null $store
     * @return mixed
     */
    public function getSpecificcountryRequiredHousenumber($store = null)
    {
        return  $this->getConfigGeneral('specificcountry_required_housenumber', $store);
    }
}
