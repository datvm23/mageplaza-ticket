/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_OscCustomField
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
define(['underscore', 'uiRegistry'], function (_, uiRegistry) {
    'use strict';

    var specificcountry_required_housenumber = window.checkoutConfig.specificcountry_required_housenumber;
    var countries = [];
    if (_.isString(specificcountry_required_housenumber)) {
        countries = specificcountry_required_housenumber.split(',');
    }

    return {
        setupRequiedByCountry: function (fieldsetType) {
            var self = this;
            uiRegistry.async(this.getFieldName(fieldsetType, 'country_id'))(function (countryField) {
                self.checkRequiedByCountry(countryField, self.getField(fieldsetType, 'housenumber'));
                countryField.on('value', function () {
                    self.checkRequiedByCountry(countryField, self.getField(fieldsetType, 'housenumber'));
                });
            });
        },
        checkRequiedByCountry: function (countryField, housenumberField) {
            if (countryField && housenumberField) {
                var required = countries.indexOf(countryField.value()) !== -1;
                housenumberField.required(required);
                housenumberField.validation['required-entry'] = required;
            }
        },
        getFieldName: function (fieldsetType, fieldName) {
            return 'checkout.steps.shipping-step.' + fieldsetType + 'Address.' + fieldsetType + '-address-fieldset.' + fieldName;
        },
        getField: function (fieldset, field) {
            return uiRegistry.get(this.getFieldName(fieldset, field));
        }

    }
});