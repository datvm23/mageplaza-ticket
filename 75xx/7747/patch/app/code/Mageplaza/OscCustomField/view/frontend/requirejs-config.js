/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_OscCustomField
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

var config = {};
if (window.location.href.indexOf('onestepcheckout') !== -1) {
    config = {
        config: {
            mixins: {
                'Magento_Checkout/js/view/shipping': {
                    'Mageplaza_OscCustomField/js/view/shipping-mixin': true
                },
                'Magento_Checkout/js/view/billing-address': {
                    'Mageplaza_OscCustomField/js/view/billing-address-mixin': true
                },
            }
        },
    };
}