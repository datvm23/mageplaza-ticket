/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_OscCustomField
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
/*global define*/
define(
    [
        'jquery',
        'underscore',
        'ko',
        'Magento_Customer/js/model/customer/address'
    ],
    function($, _, ko, address) {
        "use strict";
        var isLoggedIn = ko.observable(window.isCustomerLoggedIn);

        return function (customerAddresses) {
            return _.extend(customerAddresses, {
                getAddressItems: function () {
                    var items = [];
                    if (isLoggedIn) {
                        var customerData = window.customerData;
                        if (Object.keys(customerData).length) {
                            $.each(customerData.addresses, function (   key, item) {
                                if(item['custom_attributes'] && item['custom_attributes']['housenumber']) {
                                    item['housenumber'] = item['custom_attributes']['housenumber']['value'];
                                }
                                items.push(new address(item));
                            });
                        }
                    }
                    return items;
                }
            })
        }
    }
);
