/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_OscCustomField
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
/*global define*/
define(
    [
        'underscore',
        'mage/utils/wrapper'
    ],
    function (_, wrapper) {
        'use strict';

        return function (createBillingAddress) {
            return wrapper.wrap(createBillingAddress, function (originalAction, addressData) {
                var _addressData = addressData;
                if(addressData['customAttributes'] && addressData['customAttributes']['housenumber']) {
                    _addressData = _.extend({}, addressData, {
                        'housenumber': addressData['customAttributes']['housenumber']['value']
                    });
                }

                return createBillingAddress(_addressData);
            })
        }
    }
);
