/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_OscCustomField
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
define(
    [
        'jquery',
        'underscore',
        'Mageplaza_OscCustomField/js/model/country-housenumber-service',
        'uiRegistry'
    ],
    function ($,
              _,
              countryHousenumberService,
              uiRegistry) {

        return function (ShippingComponent) {
            return ShippingComponent.extend({
                initialize: function () {
                    this._super();

                    countryHousenumberService.setupRequiedByCountry('shipping');
                    uiRegistry.async('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.street.1')(function (streetLineTwo) {
                        streetLineTwo.visible(0);
                    });

                    return this;
                },
            })
        }
    });