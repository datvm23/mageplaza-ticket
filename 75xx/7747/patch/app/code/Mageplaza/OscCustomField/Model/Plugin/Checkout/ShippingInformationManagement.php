<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_OscCustomField
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\OscCustomField\Model\Plugin\Checkout;

use Magento\Quote\Api\CartRepositoryInterface;

class ShippingInformationManagement
{
    /**
     * @var CartRepositoryInterface
     */
    protected $_cartRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * ShippingInformationManagement constructor.
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(CartRepositoryInterface $cartRepository, \Psr\Log\LoggerInterface $logger)
    {
        $this->_cartRepository = $cartRepository;
        $this->_logger = $logger;
    }

    /**
     * @param \Magento\Checkout\Api\ShippingInformationManagementInterface $subject
     * @param \Closure $proceed
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     * @return \Magento\Checkout\Api\Data\PaymentDetailsInterface
     */
    public function aroundSaveAddressInformation(
        \Magento\Checkout\Api\ShippingInformationManagementInterface $subject,
        \Closure $proceed,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    )
    {
        try {
            /** @var \Magento\Quote\Model\Quote $quote */
            $quote = $this->_cartRepository->getActive($cartId);
            $shippingAddress = $addressInformation->getShippingAddress();
            $billingAddress = $addressInformation->getBillingAddress();

            if($shippingAddress
                && $shippingAddress->getExtensionAttributes()
                && $quote->getShippingAddress()) {
                $quote->getShippingAddress()->setData(
                    'housenumber',
                    $shippingAddress->getExtensionAttributes()->getHousenumber()
                );
            }

            if($billingAddress
                && $billingAddress->getExtensionAttributes()
                && $quote->getBillingAddress()) {
                $quote->getBillingAddress()->setData(
                    'housenumber',
                    $billingAddress->getExtensionAttributes()->getHousenumber()
                );
            }
            $this->_cartRepository->save($quote);
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }

        return $proceed($cartId, $addressInformation);
    }
}