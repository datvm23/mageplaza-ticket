<?php
/**
 * Created by PhpStorm.
 * User: zero
 * Date: 04/08/2017
 * Time: 09:05
 */

namespace Mageplaza\OscCustomField\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class DefaultConfigProvider implements ConfigProviderInterface
{
    /**
     * @type \Mageplaza\OscCustomField\Helper\Config
     */
    protected $_oscConfig;

    /**
     * DefaultConfigProvider constructor.
     * @param \Mageplaza\OscCustomField\Helper\Config $oscConfig
     */
    public function __construct(\Mageplaza\OscCustomField\Helper\Config $oscConfig)
    {
        $this->_oscConfig = $oscConfig;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        if (!$this->_oscConfig->isOscPage()) {
            return [];
        }

        $output = [
            'specificcountry_required_housenumber' => $this->_oscConfig->getSpecificcountryRequiredHousenumber()
        ];

        return $output;
    }
}