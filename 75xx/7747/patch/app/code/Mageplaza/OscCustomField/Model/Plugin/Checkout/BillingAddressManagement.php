<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_OscCustomField
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\OscCustomField\Model\Plugin\Checkout;

class BillingAddressManagement
{
    /**
     * @param \Magento\Quote\Api\BillingAddressManagementInterface $subject
     * @param \Closure $proceed
     * @param $cartId
     * @param \Magento\Quote\Api\Data\AddressInterface $address
     * @param bool $useForShipping
     * @return mixed
     */
    public function aroundAssign(
        \Magento\Quote\Api\BillingAddressManagementInterface $subject,
        \Closure $proceed,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address,
        $useForShipping = false
    )
    {
        if($address->getExtensionAttributes()) {
            $address->setData('housenumber', $address->getExtensionAttributes()->getHousenumber());
        }

        return $proceed($cartId, $address, $useForShipping);
    }
}