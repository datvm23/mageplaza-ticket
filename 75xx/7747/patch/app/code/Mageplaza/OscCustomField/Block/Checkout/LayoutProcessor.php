<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
namespace Mageplaza\OscCustomField\Block\Checkout;

class LayoutProcessor implements \Magento\Checkout\Block\Checkout\LayoutProcessorInterface
{
    /**
     * @type \Mageplaza\Osc\Helper\Data
     */
    private $_oscHelper;

    /**
     * LayoutProcessor constructor.
     * @param \Mageplaza\Osc\Helper\Data $oscHelper
     */
    public function __construct(\Mageplaza\Osc\Helper\Data $oscHelper)
    {
        $this->_oscHelper = $oscHelper;
    }

    /**
     * Process js Layout of block
     *
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        if (!$this->_oscHelper->isOscPage()) {
            return $jsLayout;
        }

        /** Shipping address fields */
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']['children'])) {
            $fields                                               = $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']
            ['children']['shipping-address-fieldset']['children'];
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']
            ['children']['shipping-address-fieldset']['children'] = $this->addAddressOption($fields, 'shippingAddress');
        }

        /** Billing address fields */
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['billingAddress']['children']['billing-address-fieldset']['children'])) {
            $fields                                              = $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['billingAddress']
            ['children']['billing-address-fieldset']['children'];
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['billingAddress']
            ['children']['billing-address-fieldset']['children'] = $this->addAddressOption($fields, 'billingAddress');
        }

        return $jsLayout;
    }

    /**
     * @param $fields
     * @return $this
     */
    private function addAddressOption($fields, $type)
    {
        $fields['housenumber'] = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => $type,
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'additionalClasses' => 'col-mp mp-6 housenumber',
            ],
            'dataScope' => $type.'.street.1',
            'label' => 'House Number',
            'provider' => 'checkoutProvider',
            'sortOrder' => isset($fields['street']['sortOrder']) ? $fields['street']['sortOrder'] + 1 : 75,
            'validation' => [
                'required-entry' => true
            ],
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        return $fields;
    }
}