<?php
/**
 * Created by PhpStorm.
 * User: zero
 * Date: 24/08/2017
 * Time: 10:16
 */

namespace Mageplaza\OscCustomField\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\AttributeRepository;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Sales\Api\OrderAddressRepositoryInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var AttributeRepository
     */
    protected $attributeRepository;

    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var OrderAddressRepositoryInterface
     */
    protected $orderAddressRepository;

    /**
     * UpgradeData constructor.
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeRepository $attributeRepository
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param OrderAddressRepositoryInterface $orderAddressRepository
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeRepository $attributeRepository,
        OrderCollectionFactory $orderCollectionFactory,
        OrderAddressRepositoryInterface $orderAddressRepository
    )
    {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeRepository = $attributeRepository;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderAddressRepository = $orderAddressRepository;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var \Magento\Framework\DB\Adapter\AdapterInterface $connection */
        $connection = $setup->getConnection();
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $setup->startSetup();
        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection */
        $orderCollection = $this->orderCollectionFactory->create();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            /** @var \Magento\Sales\Model\Order $item */
            foreach ($orderCollection as $item) {
                $this->_convertHousenumberToStreet($item->getShippingAddress());
                $this->_convertHousenumberToStreet($item->getBillingAddress());
            }

            $houseNumberAttribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'housenumber');
            $connection->delete(
                $setup->getTable('customer_form_attribute'),
                ['attribute_id =?' => $houseNumberAttribute->getAttributeId()]
            );

            $tableNames = [
                'customer_address_entity',
                'quote_address',
                'sales_order_address'
            ];

            foreach ($tableNames as $tableName) {
                $connection->dropColumn(
                    $setup->getTable($tableName),
                    'housenumber'
                );
            }
            $customerSetup->removeAttribute('customer_address', 'housenumber');
        }

        $setup->endSetup();
    }

    /**
     * @param \Magento\Sales\Model\Order\Address $address
     */
    protected function _convertHousenumberToStreet(\Magento\Sales\Model\Order\Address $address)
    {
        if($address->getData('housenumber')) {
            $street = $address->getStreet();
            $street[1] = $address->getData('housenumber');
            $address->setStreet($street);
            $this->orderAddressRepository->save($address);
        }
    }
}