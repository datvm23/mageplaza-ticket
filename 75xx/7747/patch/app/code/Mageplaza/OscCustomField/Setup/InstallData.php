<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mageplaza\OscCustomField\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\AttributeRepository;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var AttributeRepository
     */
    protected $attributeRepository;

    /**
     * @param QuoteSetupFactory $quoteSetupFactory
     * @param SalesSetupFactory $salesSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeRepository $attributeRepository
    )
    {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var \Magento\Framework\DB\Adapter\AdapterInterface $connection */
        $connection = $setup->getConnection();
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $customerSetup->removeAttribute('customer_address', 'housenumber');
        $customerSetup->addAttribute('customer_address', 'housenumber', [
            'type' => 'static',
            'label' => 'House Number',
            'input' => 'text',
            'position' => 75,
            'visible' => true,
            'visible_on_front' => true,
            'user_defined' => false,
            'required' => false,
            'system' => 0,
        ]);

        $houseNumberAttribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'housenumber');
        $formCodes = ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address'];
        $houseNumberAttribute->setData(
            'used_in_forms',
            $formCodes
        );
        $this->attributeRepository->save($houseNumberAttribute);


        $data = [];
        $houseNumberAttributeId = $houseNumberAttribute->getAttributeId();
        foreach ($formCodes as $formCode) {
            $data[] = ['form_code' => $formCode, 'attribute_id' => $houseNumberAttributeId];
        }
        $connection->insertOnDuplicate(
            $setup->getTable('customer_form_attribute'),
            $data,
            ['form_code', 'attribute_id']
        );

        $tableNames = [
            'customer_address_entity',
            'quote_address',
            'sales_order_address'
        ];

        foreach ($tableNames as $tableName) {
            $connection->addColumn(
                $setup->getTable($tableName),
                'housenumber',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null,
                    'length' => 255,
                    'comment' => 'House Number',
                ]
            );
        }

        $setup->endSetup();
    }
}
