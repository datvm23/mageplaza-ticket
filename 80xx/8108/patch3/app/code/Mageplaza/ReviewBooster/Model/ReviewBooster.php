<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ReviewBooster
 * @copyright   Copyright (c) 2017 Mageplaza (https://www.mageplaza.com/)
 * @license     http://mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ReviewBooster\Model;

use Magento\Framework\App\Area;

/**
 * Class ReviewBooster
 * @package Mageplaza\ReviewBooster\Model
 */
class ReviewBooster
{
    /**
     * @var \Mageplaza\ReviewBooster\Helper\Data
     */
    private $reviewBoosterData;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * Date model
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var \Mageplaza\ReviewBooster\Model\LogsFactory
     */
    private $logsFactory;

    /**
     * @var \Magento\Framework\Mail\Template\FactoryInterface
     */
    private $templateFactory;

    /**
     * @var \Magento\Newsletter\Model\SubscriberFactory
     */
    private $subscriberFactory;

    /**
     * @var \Mageplaza\ReviewBooster\Model\ResourceModel\Logs\CollectionFactory
     */
    private $logsCollectionFactory;

    /**
     * @param \Mageplaza\ReviewBooster\Helper\Data $reviewBoosterData
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Mageplaza\ReviewBooster\Model\LogsFactory $logsFactory
     * @param \Magento\Framework\Mail\Template\FactoryInterface $templateFactory
     * @param \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory
     * @param \Mageplaza\ReviewBooster\Model\ResourceModel\Logs\CollectionFactory $logsCollectionFactory
     */
    public function __construct(
        \Mageplaza\ReviewBooster\Helper\Data $reviewBoosterData,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Mageplaza\ReviewBooster\Model\LogsFactory $logsFactory,
        \Magento\Framework\Mail\Template\FactoryInterface $templateFactory,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Mageplaza\ReviewBooster\Model\ResourceModel\Logs\CollectionFactory $logsCollectionFactory
    ) {
        $this->reviewBoosterData = $reviewBoosterData;
        $this->date = $date;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->logsFactory = $logsFactory;
        $this->templateFactory = $templateFactory;
        $this->subscriberFactory = $subscriberFactory;
        $this->logsCollectionFactory = $logsCollectionFactory;
    }

    /**
     * @return void
     */
    public function prepareForAbandonedCart($order)
    {
        $configs = $this->reviewBoosterData->getEmailConfig();
        if (!empty($configs)) {
            $updatedAt = strtotime($order->getUpdatedAt());
            $shippingAddress = $order->getShippingAddress();
            $billingAddress = $order->getBillingAddress();
            $sequence = 1;
            if ($order->getCustomerId()) {
                $customerName = $order->getCustomerName();
            } else if ($shippingAddress->getFirstname() && $shippingAddress->getLastname()) {
                $customerName = $shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname();
            } else if ($billingAddress->getFirstname() && $billingAddress->getLastname()) {
                $customerName = $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
            } else {
                $customerName = __('Guest');
            }
            foreach ($configs as $config) {
                $time = $updatedAt + $config['send'];
                $template = $this->createEmail($order, $config, $customerName);
                $this->logsFactory->create()->saveLogs(
                    $config,
                    $order,
                    $template->processTemplate(),
                    $template->getSubject(),
                    $sequence,
                    $this->date->date('Y-m-d H:i:s', $time),
                    $customerName
                );
                $sequence++;
            }
        }
    }

    private function createEmail($order, $config, $customerName)
    {
        $customerEmail = $order->getCustomerEmail();
        $unsubscribeLink = '';
        $subscriber = $this->subscriberFactory->create()->loadByEmail($customerEmail);
        if ($subscriber->isSubscribed()) {
            $unsubscribeLink = $subscriber->getUnsubscriptionLink();
        }
        $store = $this->storeManager->getStore($order->getStoreId());
        $vars = [
            'store' => $store,
            'customer_name' => ucfirst($customerName),
            'sender' => $config['sender'],
            'order' => $order,
            'unsubscribe_link' => $unsubscribeLink
        ];
        $template = $this->templateFactory->get(
            $config['template']
        )->setVars($vars)->setOptions(
            ['area' => Area::AREA_FRONTEND, 'store' => $store->getId()]
        );
        return $template;
    }

    /**
     * @return void
     */
    public function sendMailCron()
    {
        $logs = $this->logsCollectionFactory->create();
        $logs->addFieldToFilter('status', ['eq' => 3])
            ->addFieldToFilter('display', ['eq' => 1])
            ->addFieldToFilter('send_at', ['lteq' => $this->date->date()])
            ->getSelect()->group(array('order_id', 'customer_email', 'sequence_number'));
        foreach ($logs as $log) {
            $this->sendMail($log);
        }
    }

    /**
     * @param \Mageplaza\ReviewBooster\Model\Logs $log
     * @return void
     */
    public function sendMailNow($log)
    {
        $this->sendMail($log);
    }

    /**
     * @param \Mageplaza\ReviewBooster\Model\Logs $log
     * @return void
     */
    private function sendMail($log)
    {
        try {
            $store = $this->storeManager->getStore();
            $this->transportBuilder->setTemplateIdentifier('send_again')
                ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => $store->getId()])
                ->setTemplateVars(
                    [
                        'body' => htmlspecialchars_decode($log->getEmailContent()),
                        'subject' => $log->getSubject()
                    ]
                )
                ->setFrom($log->getSender())
                ->addTo($log->getCustomerEmail(), $log->getCustomerName())
                ->getTransport()
                ->sendMessage();
            $log->setStatus(true);
        } catch (\Exception $e) {
            $log->setStatus(false);
            $this->logger->error($e->getMessage());
        }
        $log->save();
    }
}
