<?php
namespace Mageplaza\ReviewBooster\Controller\CreateLogs;

/**
 * Class Index
 * @package Mageplaza\ReviewBooster\Controller\CreateLogs
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Mageplaza\ReviewBooster\Model\ReviewBooster
     */
    private $reviewBoosterModel;

    /**
     * @var \Mageplaza\ReviewBooster\Helper\Data
     */
    private $helperData;

    /**
     * @var \Magento\Sales\Model\Order
     */
    private $order;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @param \Mageplaza\ReviewBooster\Model\ReviewBooster $reviewBoosterModel
     * @param \Mageplaza\ReviewBooster\Helper\Data $helperData
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Mageplaza\ReviewBooster\Model\ReviewBooster $reviewBoosterModel,
        \Mageplaza\ReviewBooster\Helper\Data $helperData,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        parent::__construct($context);
        $this->reviewBoosterModel = $reviewBoosterModel;
        $this->helperData = $helperData;
        $this->order = $order;
        $this->date = $date;
    }


    public function execute()
    {
        $resource = $this->_objectManager->get(\Magento\Framework\App\ResourceConnection::class);
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('sales_order');
        $sql = "SELECT entity_id FROM " . $tableName . " WHERE created_at >= '" . $this->convertDate('04/13/2018') . "'"; // mm/dd/yyyy
//        $tableName = $resource->getTableName('mageplaza_reviewbooster_logs');
//        $sql = "SELECT id, subject, customer_email, sender, order_id, sequence_number, send_at, status, display, created_at, updated_at FROM " . $tableName . " GROUP BY order_id, customer_email, sequence_number";
        $result = $connection->fetchAll($sql);
//        \Zend_Debug::dump($result);
//        die;

        foreach ($result as $key => $value) {
            $this->order->load($value['entity_id']);

            $excludeSkus = $this->helperData->getOrderConfig('exclude');
            $includeSkus = $this->helperData->getOrderConfig('include');

            $items = [];
            if ($this->helperData->isEnabled() && $this->order->getState() == 'complete') {
                foreach ($this->order->getAllVisibleItems() as $item) {
                    if ($includeSkus && !$this->checkSku($includeSkus, $item)) {
                        continue;
                    }
                    if ($excludeSkus && $this->checkSku($excludeSkus, $item)) {
                        continue;
                    }
                    $items[] = $item;
                }
                if (!empty($items)) {
                    $this->reviewBoosterModel->prepareForAbandonedCart($this->order);
                }
            }
        }
        die;
    }

    public function checkSku($skuArray, $item)
    {
        foreach ($skuArray as $sku) {
            if ((strpos($sku, '*') !== false && strpos($item->getSku(), substr($sku, 0, strlen($sku) - 1)) === 0) || $item->getSku() == $sku) {
                return true;
            }
        }
        return false;
    }

    public function convertDate($date)
    {
        return $this->date->date('Y-m-d H:i:s', strtotime($date));
    }
}
