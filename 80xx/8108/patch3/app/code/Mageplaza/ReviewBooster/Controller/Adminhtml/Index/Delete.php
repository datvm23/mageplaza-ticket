<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ReviewBooster
 * @copyright   Copyright (c) 2017 Mageplaza (https://www.mageplaza.com/)
 * @license     http://mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ReviewBooster\Controller\Adminhtml\Index;

/**
 * Class Delete
 * @package Mageplaza\ReviewBooster\Controller\Adminhtml\Index
 */
class Delete extends \Mageplaza\ReviewBooster\Controller\Adminhtml\ReviewBooster
{
    /**
     * Delete log
     *
     * @return void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            if ($id) {
                $log = $this->logsFactory->create()->load($id);
                if ($log->getId()) {
                    $resource = $this->_objectManager->get(\Magento\Framework\App\ResourceConnection::class);
                    $connection = $resource->getConnection();
                    $tableName = $resource->getTableName('mageplaza_reviewbooster_logs');

                    $sql = "SELECT id, customer_email, order_id, sequence_number, display FROM " . $tableName;
                    $result = $connection->fetchAll($sql);
                    $logModel = $this->logsFactory->create();
                    foreach ($result as $record) {
                        if ($log->getOrderId() == $record['order_id']
                            && $log->getCustomerEmail() == $record['customer_email']
                            && $log->getSequenceNumber() == $record['sequence_number']) {
                            $logModel->load($record['id'])->setDisplay(false)->save();
                        }
                    }

                    $this->messageManager->addSuccess(__('Delete success.'));
                }
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Error.'));
        }
        $this->_redirect('reviewbooster/*/logs');
    }
}
