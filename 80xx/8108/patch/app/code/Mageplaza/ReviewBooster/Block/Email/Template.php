<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ReviewBooster
 * @copyright   Copyright (c) 2017 Mageplaza (https://www.mageplaza.com/)
 * @license     http://mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\ReviewBooster\Block\Email;

/**
 * Class Template
 * @package Mageplaza\ReviewBooster\Block\Email
 */
class Template extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $imageHelper;

    /**
     * @var \Mageplaza\ReviewBooster\Helper\Data
     */
    private $helperData;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterfaceFactory
     */
    private $_productRepositoryFactory;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Mageplaza\ReviewBooster\Helper\Data $helperData
     * @param \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Mageplaza\ReviewBooster\Helper\Data $helperData,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->imageHelper = $context->getImageHelper();
        $this->helperData = $helperData;
        $this->_productRepositoryFactory = $productRepositoryFactory;
    }

    /**
     * Get items in order
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProductCollection()
    {
        $order = $this->getOrder();
        $items = [];
        $excludeSkus = $this->helperData->getOrderConfig('exclude');
        $includeSkus = $this->helperData->getOrderConfig('include');
        if ($order) {
            foreach ($order->getAllVisibleItems() as $item) {
                if ($includeSkus && !$this->checkSku($includeSkus, $item)) {
                    continue;
                }
                if ($excludeSkus && $this->checkSku($excludeSkus, $item)) {
                    continue;
                }
                $items[] = $this->_productRepositoryFactory->create()->getById($item->getProductId());
            }
        }
        return $items;
    }

    public function checkSku($skuArray, $item)
    {
        foreach ($skuArray as $sku) {
            if ((strpos($sku, '*') !== false && strpos($item->getSku(), substr($sku, 0, strlen($sku) - 1)) === 0) || $item->getSku() == $sku) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get image url in order
     *
     * @param \Magento\Catalog\Model\Product $_item
     * @return string
     */
    public function getProductImage($_item)
    {
        return $this->imageHelper->init($_item, 'category_page_grid', ['height' => 100, 'width'=> 100])->getUrl();
    }

    /**
     * Get product url in order
     *
     * @param \Magento\Catalog\Model\Product $_item
     * @return string
     */
    public function getProductUrl($_item)
    {
        $productUrl = $_item->getUrlModel()->getUrl($_item);
        if ($this->helperData->getAnalyticsConfig()) {
            $productUrl .= $this->helperData->getAnalyticsConfig();
        }
        return $productUrl;
    }

    /**
     *
     * @param \Magento\Catalog\Model\Product $_item
     * @return string
     */
    public function getReviewLink($_item)
    {
        $reviewLink = $this->helperData->getUrlEmail(
            'review/product/list',
            [
                'id' => $_item->getId(),
                'rb' => 1
            ]
        );
        if ($this->helperData->getAnalyticsConfig()) {
            $reviewLink .= $this->helperData->getAnalyticsConfig();
        }
        return $reviewLink;
    }

}
