<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_ReviewBooster
 * @copyright   Copyright (c) 2017 Mageplaza (https://www.mageplaza.com/)
 * @license     http://mageplaza.com/LICENSE.txt
 */
namespace Mageplaza\ReviewBooster\Observer;

use Magento\Framework\Event\ObserverInterface;

class CreateLogs implements ObserverInterface
{
    /**
     * @var \Mageplaza\ReviewBooster\Model\ReviewBooster
     */
    private $reviewBoosterModel;

    /**
     * @var \Mageplaza\ReviewBooster\Helper\Data
     */
    private $helperData;

    /**
     * @param \Mageplaza\ReviewBooster\Model\ReviewBooster $reviewBoosterModel
     * @param \Mageplaza\ReviewBooster\Helper\Data $helperData
     */
    public function __construct(
        \Mageplaza\ReviewBooster\Model\ReviewBooster $reviewBoosterModel,
        \Mageplaza\ReviewBooster\Helper\Data $helperData
    ) {
        $this->reviewBoosterModel = $reviewBoosterModel;
        $this->helperData = $helperData;
    }

    /**
     * Checking whether the using static urls in WYSIWYG allowed event
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $excludeSkus = $this->helperData->getOrderConfig('exclude');
        $includeSkus = $this->helperData->getOrderConfig('include');

        $items = [];
        if ($this->helperData->isEnabled() && $order->getState() == 'complete') {
            foreach ($order->getAllVisibleItems() as $item) {
                if ($includeSkus && !$this->checkSku($includeSkus, $item)) {
                    continue;
                }
                if ($excludeSkus && $this->checkSku($excludeSkus, $item)) {
                    continue;
                }
                $items[] = $item;
            }
            if (!empty($items)) {
                $this->reviewBoosterModel->prepareForAbandonedCart($order);
            }
        }
    }

    public function checkSku($skuArray, $item)
    {
        foreach ($skuArray as $sku) {
            if ((strpos($sku, '*') !== false && strpos($item->getSku(), substr($sku, 0, strlen($sku) - 1)) === 0)
                || $item->getSku() == $sku) {
                return true;
            }
        }
        return false;
    }
}
