/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'ko',
    'Magento_Checkout/js/view/form/element/email',
    'Magento_Customer/js/model/customer',
    'Mageplaza_Osc/js/model/osc-data',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Customer/js/action/check-email-availability',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/checkout-data',
    'Mageplaza_Osc/js/model/compatible/amazon-pay',
    'Mageplaza_Osc/js/action/check-subscribed',
    'Mageplaza_Osc/js/view/review/addition/newsletter',
    'mage/url',
    'rjsResolver',
    'mage/validation'
], function ($, ko, Component, customer, oscData, additionalValidators, checkEmailAvailability, quote, checkoutData, amazonPay, checkSubscribed, newsletter, urlBuilder, resolver) {
    'use strict';

    var cacheKey = 'form_register_chechbox',
        allowGuestCheckout = window.checkoutConfig.oscConfig.allowGuestCheckout,
        passwordMinLength = window.checkoutConfig.oscConfig.register.dataPasswordMinLength,
        passwordMinCharacter = window.checkoutConfig.oscConfig.register.dataPasswordMinCharacterSets,
        customerEmailElement = '.form-login #customer-email';

    if (!customer.isLoggedIn() && !allowGuestCheckout) {
        oscData.setData(cacheKey, true);
    }

    return Component.extend({
        defaults: {
            template: 'Mageplaza_Osc/container/form/element/email',
            isLoginVisible: false
        },
        checkDelay: 2000,
        dataPasswordMinLength: passwordMinLength,
        dataPasswordMinCharacterSets: passwordMinCharacter,
        isAmazonAccountLoggedIn: amazonPay.isAmazonAccountLoggedIn,

        initialize: function () {
            this._super();

            if (!!this.email()) {
                resolver(this.emailHasChanged.bind(this));
            } else {
                this.isPasswordVisible(null);
            }

            if (customer.isLoggedIn()) {
                checkSubscribed(customer.customerData.email).done(function (isSubscribed) {
                    newsletter().isSubscribedBoxVisible(!isSubscribed);
                });
            }

            additionalValidators.registerValidator(this);
        },

        initObservable: function () {
            this._super().observe(['isCreateAccountMsg']);

            this._super()
                .observe({
                    isCheckboxRegisterVisible: allowGuestCheckout,
                    isRegisterVisible: oscData.getData(cacheKey)
                });

            this.isRegisterVisible.subscribe(function (newValue) {
                oscData.setData(cacheKey, newValue);
            });

            this.isPasswordVisible.subscribe(function (newValue) {
                console.trace(newValue);
            });

            return this;
        },

        triggerLogin: function () {
            if($('.osc-authentication-wrapper a.action-auth-toggle').hasClass('osc-authentication-toggle')){
                $('.osc-authentication-toggle').trigger('click');
            }else{
                window.location.href = urlBuilder.build("customer/account/login");
            }
        },

        /**
         * Callback on changing email property
         */
        emailHasChanged: function () {
            var self = this;

            clearTimeout(this.emailCheckTimeout);

            if (!customer.isLoggedIn() && !this.isLoading()) {
                if (self.validateEmail()) {
                    quote.guestEmail = self.email();
                    checkoutData.setValidatedEmailValue(self.email());
                }
                self.emailCheckTimeout = setTimeout(function () {
                    if (self.validateEmail()) {
                        self.checkEmailAvailability();
                        self.validateEmail(false);
                    } else {
                        self.isPasswordVisible(null);
                    }
                }, self.checkDelay);

                checkoutData.setInputFieldEmailValue(self.email());
            }
        },

        /**
         * Check email existing.
         */
        checkEmailAvailability: function () {
            var self = this;

            this.validateRequest();
            this.isEmailCheckComplete = $.Deferred();
            this.isLoading(true);
            this.checkRequest = checkEmailAvailability(this.isEmailCheckComplete, this.email());

            $.when(this.isEmailCheckComplete).done(function () {
                self.isPasswordVisible(false);
                self.isCreateAccountMsg(false);

                if (oscData.getData(cacheKey) !== false) {
                    self.isRegisterVisible(true);
                }
            }).fail(function () {
                self.isPasswordVisible(true);
                self.isCreateAccountMsg(true);
                checkoutData.setCheckedEmailValue(self.email());

                $('.popup-authentication #login-email').val(self.email());
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }).always(function () {
                self.isLoading(false);

                checkSubscribed(self.email()).done(function (isSubscribed) {
                    newsletter().isSubscribedBoxVisible(!isSubscribed);
                });
            });
        },

        validateEmail: function (focused) {
            var loginFormSelector = 'form[data-role=email-with-possible-login]',
                usernameSelector = loginFormSelector + ' input[name=username]',
                loginForm = $(loginFormSelector),
                validator;

            loginForm.validation();

            if (!$(usernameSelector).valid()) {
                this.isPasswordVisible(null);
            }

            if (focused === false) {
                this.isCreateAccountMsg(!$(usernameSelector).valid());
                return !!$(usernameSelector).valid();
            }

            validator = loginForm.validate();

            if (focused === true && !validator.check(usernameSelector)) {
                $('.mage-error[for="customer-email"]').css('display', 'none');
            }

            return validator.check(usernameSelector);
        },

        /**
         * Resolves an initial sate of a login form.
         *
         * @returns {Boolean} - initial visibility state.
         */
        resolveInitialPasswordVisibility: function () {
            if (checkoutData.getInputFieldEmailValue() !== '') {
                return checkoutData.getInputFieldEmailValue() === checkoutData.getCheckedEmailValue();
            }

            return false;
        },

        validate: function (type) {
            if (this.email()) {
                this.isPasswordVisible(this.resolveInitialPasswordVisibility());
            } else {
                this.isPasswordVisible(this.resolveInitialPasswordVisibility() ? true : null);
            }
            this.isRegisterVisible(oscData.getData(cacheKey));

            if (customer.isLoggedIn() || !this.isRegisterVisible() || this.isPasswordVisible()) {
                oscData.setData('register', false);
                return true;
            }

            if (typeof type !== 'undefined') {
                var selector = $('#osc-' + type);

                selector.parents('form').validation();

                return !!selector.valid();
            }

            if (!customer.isLoggedIn() && this.isRegisterVisible() && !this.isPasswordVisible() && this.email()) {
                this.checkEmailAvailability();
            }

            var passwordSelector = $('#osc-password');
            passwordSelector.parents('form').validation();

            var password = !!passwordSelector.valid();
            var confirm = !!$('#osc-password-confirmation').valid();

            var result = password && confirm,
                offsetHeight = $(window).height()/2;
            if (result) {
                oscData.setData('register', true);
                oscData.setData('password', passwordSelector.val());
            } else if (!password) {
                $('html, body').scrollTop(
                    $('#osc-password').offset().top - offsetHeight
                );
                $('#osc-password').focus();
            } else if (!confirm) {
                $('html, body').scrollTop(
                    $('#osc-password-confirmation').offset().top - offsetHeight
                );
                $('#osc-password-confirmation').focus();
            }

            return result;
        },

        /** Move label element when input has value */
        hasValue: function(){
            if (window.checkoutConfig.oscConfig.isUsedMaterialDesign) {
                $(customerEmailElement).val() ? $(customerEmailElement).addClass('active') : $(customerEmailElement).removeClass('active');
            }
        }
    });
});
