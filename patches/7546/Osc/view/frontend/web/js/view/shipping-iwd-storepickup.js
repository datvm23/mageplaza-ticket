/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2017 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'mage/translate',
    'underscore',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/model/payment-service',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/quote',
    'ko',
    'jquery',
], function (
    $t,
    _,
    selectShippingMethodAction,
    paymentService,
    checkoutData,
    quote,
    ko,
    $
) {
    var instance = null;
    
    return function (Shipping) {
        return Shipping.extend({
            defaults: {
                template: 'Mageplaza_Osc/container/shipping'
            },
            selectedStore: ko.observable(null),
            initialize: function () {
                this._super();
                return this;
            },
            isEnableModuleIWDStorePickup: window.checkoutConfig.oscConfig.compatible.isEnableModuleIWDStorePickup,
            initObservable: function () {
                instance = this;
                this._super();
                this.selectedStore.subscribe(function (value) {
                    if (value === null) {
                        return;
                    }

                    if (value) {
                        $('#iwd_storepickup_store_select').addClass('has_value');
                        if ($('input[value="' + value + '"]').length) {
                            $('input[value="' + value + '"]').trigger('click');
                        }
                    } else {
                        $('#iwd_storepickup_store_select').removeClass('has_value');
                        $('.iwd_store_pickup_container_checkout input').prop('checked', false);
                        selectShippingMethodAction(null);
                        checkoutData.setSelectedShippingRate(null);
                        paymentService.setPaymentMethods([]);
                    }
                });

                return this;
            },
            selectShippingMethod: function (shippingMethod) {
                if (shippingMethod.carrier_code !== 'iwdstorepickup' && $('#iwd_storepickup_store_select').length) {
                    instance.selectedStore(null);
                }

                selectShippingMethodAction(shippingMethod);
                checkoutData.setSelectedShippingRate(shippingMethod.carrier_code + '_' + shippingMethod.method_code);
                return true;
            },
            storePickupOptionsCaption: function () {
                if (this.rates().length === 1) {
                    return null;
                }

                return $.mage.__('Choose a store...');
            },
            pickupStores: function () {
                return _.filter(this.rates(), function (rate) {
                    return 'iwdstorepickup' === rate['carrier_code'];
                });
            },
            afterSelectRenders: function () {
                if ($('.iwd_storepickup_store_select').length > 1) {
                    $('.iwd_storepickup_store_select').slice(1).closest('td').remove();
                }
            }
        });
    }
});