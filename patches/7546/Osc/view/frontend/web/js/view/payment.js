/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2017 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/view/payment',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Mageplaza_Osc/js/model/checkout-data-resolver',
        'Mageplaza_Osc/js/model/payment-service',
        'rjsResolver',
        'breakjs',
        'sticky-kit',
    ],
    function (
        ko,
        $,
        Component,
        quote,
        stepNavigator,
        additionalValidators,
        oscDataResolver,
        oscPaymentService,
        rjsResolver,
        Breakjs
    ) {
        'use strict';

        oscDataResolver.resolveDefaultPaymentMethod();

        return Component.extend({
            defaults: {
                template: 'Mageplaza_Osc/container/payment'
            },
            isLoading: oscPaymentService.isLoading,
            errorValidationMessage: ko.observable(false),

            initialize: function () {
                var self = this;

                this._super();

                stepNavigator.steps.removeAll();

                additionalValidators.registerValidator(this);

                quote.paymentMethod.subscribe(function () {
                    self.errorValidationMessage(false);
                });

                this.layoutMedia = Breakjs({
                    mobile: 481,
                    tablet: 768,
                    desktop: 992
                });

                rjsResolver(this.stickyOrderSidebar.bind(this));

                return this;
            },

            stickyOrderSidebar: function () {
                var layoutMedia = this.layoutMedia;
                var $opcSidebar = $('#opc-sidebar');
                if(layoutMedia.atMost('mobile')) {
                    $opcSidebar.trigger("sticky_kit:detach");
                } else {
                    $opcSidebar.stick_in_parent({parent: $('body')});
                }

                this.layoutMedia.addChangeListener(function(layout) {
                    if(layoutMedia.atMost('mobile')) {
                        $opcSidebar.trigger("sticky_kit:detach");
                    } else {
                        $opcSidebar.stick_in_parent({parent: $('body')});
                    }
                });
            },

            validate: function () {
                if (!quote.paymentMethod()) {
                    this.errorValidationMessage('Please specify a payment method.');

                    return false;
                }

                return true;
            }
        });
    }
);
