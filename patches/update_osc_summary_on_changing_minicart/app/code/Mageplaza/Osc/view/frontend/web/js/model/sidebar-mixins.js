define([
    'jquery',
    'Magento_Customer/js/model/authentication-popup',
    'Magento_Customer/js/customer-data',
	'Mageplaza_Osc/js/view/summary/item/details',
    'Magento_Ui/js/modal/alert',
    'Magento_Ui/js/modal/confirm',
    'jquery/ui',
    'mage/decorate',
    'mage/collapsible',
    'mage/cookies'
], function ($, authenticationPopup, customerData, details, alert, confirm) {
    'use strict';

    return function (widget) {

        $.widget('mage.sidebar', widget, {
            /**
             * @param {HTMLElement} elem
             * @private
             */
            _updateItemQty: function (elem) {
                var itemId = elem.data('cart-item');
                this._ajax(this.options.url.update, {
                    'item_id': itemId,
                    'item_qty': $('#cart-item-' + itemId + '-qty').val()
                }, elem, this._updateItemQtyAfter);
				details().updateItem(itemId,$('#cart-item-' + itemId + '-qty').val());
            },
			/**
			 * @param {HTMLElement} elem
			 * @private
			 */
			_removeItem: function (elem) {
				var itemId = elem.data('cart-item');
				this._ajax(this.options.url.remove, {
					'item_id': itemId
				}, elem, this._removeItemAfter);
				details().updateItem(-999);
			}
        });

        return $.mage.sidebar;
    }
});