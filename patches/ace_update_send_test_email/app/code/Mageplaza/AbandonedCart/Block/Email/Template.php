<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AbandonedCart
 * @copyright   Copyright (c) 2017-2018 Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AbandonedCart\Block\Email;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Directory\Model\PriceCurrency;
use Mageplaza\AbandonedCart\Helper\Data as ModuleHelper;
use Magento\Tax\Helper\Data as TaxHelper;

/**
 * Class Template
 * @package Mageplaza\AbandonedCart\Block\Email
 */
class Template extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $imageHelper;

    /**
     * @var \Magento\Directory\Model\PriceCurrency
     */
    protected $priceCurrency;

    /**
     * @var \Mageplaza\AbandonedCart\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Magento\Tax\Helper\Data
     */
    protected $taxHelper;

    /**
     * Template constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Directory\Model\PriceCurrency $priceCurrency
     * @param \Mageplaza\AbandonedCart\Helper\Data $helperData
     * @param \Magento\Tax\Helper\Data $taxHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        PriceCurrency $priceCurrency,
        ModuleHelper $helperData,
        TaxHelper $taxHelper,
        array $data = []
    )
    {
        parent::__construct($context, $data);

        $this->_productRepository = $productRepository;
        $this->imageHelper        = $context->getImageHelper();
        $this->priceCurrency      = $priceCurrency;
        $this->helperData         = $helperData;
        $this->taxHelper          = $taxHelper;
    }

    /**
     * Get items in quote
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductCollection()
    {
        $items = [];
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->getQuote();
        if ($quote) {
            foreach ($quote->getAllVisibleItems() as $item) {
                $items[] = $this->_productRepository->getById($item->getProductId())
                    ->setQtyOrder($item->getQty());
            }
        }

        return $items;
    }

    /**
     * @param $area
     * @return bool
     */
    public function isSubtotalDisplayed($area)
    {
        $storeId = $this->_storeManager->getStore()->getId();
        if ($area == 'excl_tax') {
            return $this->taxHelper->getConfig()->displayCartSubtotalBoth($storeId) || $this->taxHelper->getConfig()->displayCartSubtotalExclTax($storeId);
        } else if ($area == 'incl_tax') {
            return $this->taxHelper->getConfig()->displayCartSubtotalBoth($storeId) || $this->taxHelper->getConfig()->displayCartSubtotalInclTax($storeId);
        }

        return false;
    }

    /**
     * Get subtotal in quote
     *
     * @param $area
     * @return float|string
     */
    public function getSubtotal($area = '')
    {
        $subtotal = 0;
        if ($quote = $this->getQuote()) {
            $subtotal = $quote->getTotals()['subtotal']['value_' . $area] ?: $quote->getTotals()['subtotal']['value'];
        }

        return $this->priceCurrency->format($subtotal, true);
    }

    /**
     * Get image url in quote
     *
     * @param $_item
     * @return string
     */
    public function getProductImage($_item)
    {
        $imageUrl = $this->imageHelper->init($_item, 'category_page_grid', ['height' => 100, 'width' => 100])->getUrl();
        return str_replace('\\', '/', $imageUrl);
    }

    /**
     * Get item price in quote
     *
     * @param $_item
     * @return float|string
     */
    public function getProductPrice($_item)
    {
        $productPrice = $_item->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();

        return $this->priceCurrency->format($productPrice, false);
    }

    /**
     * @return string
     */
    public function getPlaceholderImage()
    {
        return $this->imageHelper->getDefaultPlaceholderUrl('image');
    }
}
