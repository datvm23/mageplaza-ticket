<?php
namespace Mageplaza\AbandonedCart\Controller\Adminhtml\Index;

use Magento\Framework\App\Action\Context;
use Magento\Quote\Api\CartRepositoryInterface;
use Mageplaza\AbandonedCart\Helper\Data;
use Magento\Framework\Math\Random;
use Mageplaza\AbandonedCart\Model\AbandonedCart;
use Psr\Log\LoggerInterface;

/**
 * Class Test
 * @package Mageplaza\AbandonedCart\Controller\Adminhtml\Index
 */
class Test extends \Magento\Framework\App\Action\Action
{
    /**
     * @var CartRepositoryInterface
     */
    protected $quoteFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Random
     */
    protected $mathRandom;

    /**
     * @var AbandonedCart
     */
    protected $abandonedCart;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Mageplaza\AbandonedCart\Model\Token
     */
    protected $abandonedCartToken;

    /**
     * @var \Magento\Quote\Model\ResourceModel\Quote\CollectionFactory
     */
    protected $quoteCollection;

    /**
     * Test constructor.
     * @param Context $context
     * @param CartRepositoryInterface $quoteFactory
     * @param Data $helper
     * @param LoggerInterface $logger
     * @param Random $mathRandom
     * @param AbandonedCart $abandonedCart
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Mageplaza\AbandonedCart\Model\Token $abandonedCartToken
     * @param \Magento\Quote\Model\ResourceModel\Quote\CollectionFactory $quoteCollection
     */
    public function __construct(
        Context $context,
        CartRepositoryInterface $quoteFactory,
        Data $helper,
        LoggerInterface $logger,
        Random $mathRandom,
        AbandonedCart $abandonedCart,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Mageplaza\AbandonedCart\Model\Token $abandonedCartToken,
        \Magento\Quote\Model\ResourceModel\Quote\CollectionFactory $quoteCollection
    )
    {
        parent::__construct($context);
        $this->quoteFactory            = $quoteFactory;
        $this->helper                  = $helper;
        $this->logger                  = $logger;
        $this->mathRandom              = $mathRandom;
        $this->abandonedCart           = $abandonedCart;
        $this->orderFactory            = $orderFactory;
        $this->jsonHelper              = $jsonHelper;
        $this->abandonedCartToken      = $abandonedCartToken;
        $this->quoteCollection         = $quoteCollection;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result['status'] = false;
        $storeId = $this->getRequest()->getParam('backend_store');

        if (!$this->helper->isEnabled($storeId ?: null)) {
            $result['content'] = __('Please enable the extension and save config before sending test emails.');
        } else {
            try {
                /** @var \Magento\Quote\Model\ResourceModel\Quote\Collection $collection */
                $collection = $this->quoteCollection->create()->addFieldToFilter('is_active', ['eq' => 1])->addFieldToFilter('items_count', ['gt' => 0]);
                if ($storeId) {
                    $collection->addFieldToFilter('store_id', $storeId);
                }

                /** @var \Magento\Quote\Model\Quote $quote */
                $quote = $this->quoteFactory->get($collection->getFirstItem()->getId());
                $quote->setStoreId($collection->getFirstItem()->getStoreId());

                $config['sender'] = $this->getRequest()->getParam('sender');
                $config['template'] = $this->getRequest()->getParam('template');
                $config['test_email'] = $this->getRequest()->getParam('test_email');

                $coupon = [];
                if ((bool)$this->getRequest()->getParam('coupon')) {
                    $coupon = $this->abandonedCart->createCoupon($quote->getStoreId());
                }

                $this->abandonedCart->sendMail($quote, $config, 'test_email', $coupon);

                $result['status'] = true;
                $result['content'] = __('Sent successfully! Please check your email box.');
            } catch (\Exception $e) {
                $result['content'] = $e->getMessage();
                if (strpos($result['content'], 'No such entity') !== false) {
                    $result['content'] .= '<br/> Please create a cart in this store to test';
                }
                $this->logger->critical($e);
            }
        }

        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($result)
        );
    }
}
