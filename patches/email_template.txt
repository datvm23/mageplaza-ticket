You can add the code into the Email Template in Magento 2.
Below this is the list of some variables for you:
osc_order_comment
osc_delivery_time
osc_order_house_security_code
gift_wrap_type
osc_gift_wrap_amount
base_osc_gift_wrap_amount
osc_survey_question
osc_survey_answers
Ex: This is the code to add the comment in Email Template:

{{if order.getOscOrderComment()}}
   <p>Your order comment is: {{trans "%comment" comment=$order.getOscOrderComment()}} </p>
{{/if}}
