<?php

namespace Mageplaza\Osc\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class CheckoutSuccess implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Indexer\IndexerRegistry
     */
    protected $indexerRegistry;

    /**
     * @var \Magento\Checkout\Model\Type\Onepage
     */
    protected $onepage;

    /**
     * CheckoutSuccess constructor.
     * @param \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry
     * @param \Magento\Checkout\Model\Type\Onepage $onepage
     */
    public function __construct(
        \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry,
        \Magento\Checkout\Model\Type\Onepage $onepage)
    {
        $this->indexerRegistry = $indexerRegistry;
        $this->onepage = $onepage;
    }

    /**
     * @param EventObserver $observer
     * @throws \Exception
     */
    public function execute(EventObserver $observer)
    {
        $session = $this->onepage->getCheckout();
        if ($session->getOscRegister()) {
            $indexer = $this->indexerRegistry->get('customer_grid');
            $indexer->reindexAll();
        }
    }
}