<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mageplaza\Osc\Model\ResourceModel\Order\Grid;

/**
 * Order grid collection
 */
class Collection extends \Magento\Sales\Model\ResourceModel\Order\Grid\Collection
{
    /**
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()->joinLeft(
            $this->getTable('sales_order'),
            'sales_order.entity_id = main_table.entity_id',
            'osc_order_comment'
        );

        return $this;
    }
}
