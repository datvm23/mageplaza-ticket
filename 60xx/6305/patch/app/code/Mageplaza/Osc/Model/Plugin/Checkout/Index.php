<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2017 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Osc\Model\Plugin\Checkout;

/**
 * Class Index
 * @package Mageplaza\Osc\Model\Plugin\Checkout
 */
class Index
{
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $_resultRedirectFactory;

    /**
     * @type \Mageplaza\Osc\Helper\Data
     */
    protected $_checkoutHelper;

    /**
     * @param \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
     * @param \Mageplaza\Osc\Helper\Data $checkoutHelper
     */
    public function __construct(
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory,
        \Mageplaza\Osc\Helper\Data $checkoutHelper
    )
    {
        $this->_resultRedirectFactory   = $redirectFactory;
        $this->_checkoutHelper          = $checkoutHelper;
    }

    /**
     * @param \Closure $proceed
     * @return mixed
     */
    public function aroundExecute(
        \Magento\Checkout\Controller\Index\Index $subject,
        \Closure $proceed
    )
    {
        if (!$this->_checkoutHelper->isEnabled()) {
            return $proceed();
        }
        return $this->_resultRedirectFactory->create()->setPath('onestepcheckout');
    }
}
