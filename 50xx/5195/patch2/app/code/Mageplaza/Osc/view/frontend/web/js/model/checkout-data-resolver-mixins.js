/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'underscore',
        'uiRegistry',
        'mage/utils/wrapper'
    ], function (_, uiRegistry, wrapper) {
        'use strict';
        var resolveAddressSameAsShipping = _.once(function () {
            uiRegistry.async('checkout.steps.shipping-step.billingAddress')(function (billingAddress) {
                billingAddress.isAddressSameAsShipping(false);
                billingAddress.useShippingAddress();
            });
        });

        return function (checkoutDataResolver) {
            return _.extend(checkoutDataResolver, {
                resolveBillingAddress: wrapper.wrap(checkoutDataResolver.resolveBillingAddress, function (originalResolveBillingAddress) {
                    originalResolveBillingAddress();
                    resolveAddressSameAsShipping();
                })
            });
        };
    }
);