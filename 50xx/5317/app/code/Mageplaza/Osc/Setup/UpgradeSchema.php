<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
namespace Mageplaza\Osc\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
	/**
	 * {@inheritdoc}
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$setup->startSetup();
		$tableNews = $setup->getTable('newsletter_subscriber');

		if (version_compare($context->getVersion(), '2.1.2') < 0) {
			$setup->getConnection()->addColumn(
				$tableNews,
				'subscriber_firstname',
				[
					'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					'length'   => 255,
					'nullable' => true,
					'default'  => null,
					'comment'  => 'Subscriber First Name'
				]
			);
			$setup->getConnection()->addColumn(
				$tableNews,
				'subscriber_lastname',
				[
					'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					'length'   => 255,
					'nullable' => true,
					'default'  => null,
					'comment'  => 'Subscriber Last Name'
				]
			);
		}

		$setup->endSetup();
	}
}
