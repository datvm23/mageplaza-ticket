<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Edt\Observer;

use Magento\Framework\Event\ObserverInterface;
use Mageplaza\Edt\Helper\Data as EdtHelper;
use Magento\Checkout\Model\Session;
use Magento\Framework\Event\Observer;

/**
 * Class QuoteSubmitBefore
 * @package Mageplaza\Edt\Observer
 */
class QuoteSubmitBefore implements ObserverInterface
{
    /**
     * @var EdtHelper
     */
    protected $edtHelper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @param EdtHelper $edtHelper
     * @param Session $checkoutSession
     * @codeCoverageIgnore
     */
    public function __construct(
        EdtHelper $edtHelper,
        Session $checkoutSession
    )
    {
        $this->edtHelper       = $edtHelper;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Serializer_Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Api\Data\OrderInterface $order */
        $order = $observer->getEvent()->getOrder();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        if ($edtData = $this->checkoutSession->getEdtData()) {
            $order->setData('mp_delivery_information', $this->edtHelper->serialize($edtData));

            $this->checkoutSession->unsEdtData();
        }
    }
}
