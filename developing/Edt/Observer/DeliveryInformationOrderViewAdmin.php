<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Edt\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Mageplaza\Edt\Helper\Data as EdtHelper;

class DeliveryInformationOrderViewAdmin implements ObserverInterface
{
    /**
     * @var EdtHelper
     */
    protected $edtHelper;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Framework\View\Element\Template
     */
    protected $viewTemplate;

    /**
     * @param EdtHelper $edtHelper
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\View\Element\Template $viewTemplate
     */
    public function __construct(
        EdtHelper $edtHelper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\View\Element\Template $viewTemplate)
    {
        $this->edtHelper     = $edtHelper;
        $this->objectManager = $objectManager;
        $this->viewTemplate  = $viewTemplate;
    }

    /**
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Serializer_Exception
     */
    public function execute(EventObserver $observer)
    {
        if ($observer->getElementName() == 'order_shipping_view') {
            $orderShippingViewBlock = $observer->getLayout()->getBlock($observer->getElementName());
            $order = $orderShippingViewBlock->getOrder();

            $deliveryInformation = $this->edtHelper->unserialize($order->getMpDeliveryInformation());

            $deliveryDateBlock = $this->objectManager->create('Magento\Framework\View\Element\Template');
            $deliveryDateBlock->setDeliveryDate($deliveryInformation['deliveryDate']);
            $deliveryDateBlock->setDeliveryTime($deliveryInformation['deliveryTime']);
            $deliveryDateBlock->setHouseSecurityCode($deliveryInformation['houseSecurityCode']);
            $deliveryDateBlock->setDeliveryComment($deliveryInformation['deliveryComment']);
            $deliveryDateBlock->setTemplate('Mageplaza_Edt::order/view/delivery-information.phtml');

            $html = $observer->getTransport()->getOutput() . $deliveryDateBlock->toHtml();
            $observer->getTransport()->setOutput($html);
        }
    }
}