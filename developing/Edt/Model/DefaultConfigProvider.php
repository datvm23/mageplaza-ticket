<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Edt\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\Edt\Helper\Data as EdtHelper;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class DefaultConfigProvider implements ConfigProviderInterface
{
    /**
     * @var EdtHelper
     */
    protected $edtHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * DefaultConfigProvider constructor.
     * @param EdtHelper $edtHelper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        EdtHelper $edtHelper,
        StoreManagerInterface $storeManager
    )
    {
        $this->edtHelper                 = $edtHelper;
        $this->storeManager              = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $output = [
            'edtConfig' => $this->getEdtConfig()
        ];

        return $output;
    }

    /**
     * @return array
     * @throws \Zend_Serializer_Exception
     */
    private function getEdtConfig()
    {
        return [
            'isEnabled'                   => $this->edtHelper->isEnabled(),
            'isEnabledDeliveryTime'       => $this->edtHelper->isEnabledDeliveryTime(),
            'isEnabledHouseSecurityCode'  => $this->edtHelper->isEnabledHouseSecurityCode(),
            'isEnabledDeliveryComment'    => $this->edtHelper->isEnabledDeliveryComment(),
            'deliveryDateFormat'          => $this->edtHelper->getDateFormat(),
            'deliveryDaysOff'             => $this->edtHelper->getDaysOff(),
            'deliveryDateOff'             => $this->edtHelper->getDateOff(),
            'deliveryTime'                => $this->edtHelper->getDeliveryTIme()
        ];
    }
}
