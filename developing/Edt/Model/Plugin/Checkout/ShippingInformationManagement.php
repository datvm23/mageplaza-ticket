<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Edt\Model\Plugin\Checkout;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Checkout\Api\Data\ShippingInformationInterface;

/**
 * Class ShippingInformationManagement
 * @package Mageplaza\Edt\Model\Plugin\Checkout
 */
class ShippingInformationManagement
{
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        CheckoutSession $checkoutSession
    )
    {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param int $cartId
     * @param ShippingInformationInterface $addressInformation
     * @return array
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    )
    {
        $extensionAttributes = $addressInformation->getExtensionAttributes();
        $deliveryInformation = [
            'deliveryDate'      => $extensionAttributes->getMpDeliveryDate(),
            'deliveryTime'      => $extensionAttributes->getMpDeliveryTime(),
            'houseSecurityCode' => $extensionAttributes->getMpHouseSecurityCode(),
            'deliveryComment'   => $extensionAttributes->getMpDeliveryComment()
        ];
        $this->checkoutSession->setEdtData($deliveryInformation);

        return [$cartId, $addressInformation];
    }
}
