/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'jquery',
        'ko',
        'underscore',
        'uiComponent',
        'Mageplaza_Edt/js/model/edt-data',
        'jquery/ui',
        'jquery/jquery-ui-timepicker-addon'
    ],
    function ($, ko, _, Component, edtData) {
        'use strict';

        var cacheKeyDeliveryDate = 'deliveryDate',
            cacheKeyDeliveryTime = 'deliveryTime',
            cacheKeyHouseSecurityCode = 'houseSecurityCode',
            cacheKeyDeliveryComment = 'deliveryComment',
            dateFormat = window.checkoutConfig.edtConfig.deliveryDateFormat,
            daysOff = window.checkoutConfig.edtConfig.deliveryDaysOff,
            dateOff = [];

        function prepareSubscribeValue(object, cacheKey) {
            object(edtData.getData(cacheKey));
            object.subscribe(function (newValue) {
                edtData.setData(cacheKey, newValue);
            });
        }

        function formatDeliveryTime(time) {
            var from = time['from'][0] + 'h' + time['from'][1],
                to = time['to'][0] + 'h' + time['to'][1];
            return from + ' - ' + to;
        }

        return Component.extend({
            defaults: {
                template: 'Mageplaza_Edt/container/delivery-information'
            },
            deliveryDate: ko.observable(),
            deliveryTime: ko.observable(),
            houseSecurityCode: ko.observable(),
            deliveryComment: ko.observable(),
            deliveryTimeOptions: ko.observableArray([]),
            isVisible: ko.observable(edtData.getData(cacheKeyDeliveryDate)),

            initialize: function () {
                this._super();

                var self = this;

                $.each(window.checkoutConfig.edtConfig.deliveryDateOff, function (index, date) {
                    dateOff.push(date['date_off']);
                });

                ko.bindingHandlers.datepicker = {
                    init: function (element) {
                        var options = {
                                minDate: 0,
                                showButtonPanel: false,
                                dateFormat: dateFormat,
                                showOn: 'both',
                                buttonText: '',
                                beforeShowDay: function (date) {
                                    var checkDays = true,
                                        checkDate = true;

                                    if (daysOff) {
                                        checkDays = (daysOff.indexOf(date.getDay()) === -1);
                                    }

                                    if (dateOff) {
                                        var dateToCheck = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
                                        checkDate = !($.inArray(dateToCheck, dateOff) !== -1);
                                    }

                                    return [checkDays && checkDate];
                                }
                            };
                        $(element).datepicker(options);
                    }
                };

                $.each(window.checkoutConfig.edtConfig.deliveryTime, function (index, item) {
                    self.deliveryTimeOptions.push(formatDeliveryTime(item));
                });

                prepareSubscribeValue(this.deliveryDate, cacheKeyDeliveryDate);
                prepareSubscribeValue(this.deliveryTime, cacheKeyDeliveryTime);
                prepareSubscribeValue(this.houseSecurityCode, cacheKeyHouseSecurityCode);
                prepareSubscribeValue(this.deliveryComment, cacheKeyDeliveryComment);

                this.isVisible = ko.computed(function() {
                    return !!self.deliveryDate();
                });

                return this;
            },

            removeDeliveryDate: function () {
                if (edtData.getData(cacheKeyDeliveryDate) && edtData.getData(cacheKeyDeliveryDate) != null) {
                    this.deliveryDate('');
                }
            }
        });
    }
);
