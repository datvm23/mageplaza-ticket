/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'jquery',
        'underscore',
        'ko',
        'Magento_Checkout/js/view/shipping',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/action/set-shipping-information',
        'Mageplaza_Osc/js/action/payment-total-information',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/shipping-rate-service',
        'Magento_Checkout/js/model/shipping-service',
        'Mageplaza_Osc/js/model/checkout-data-resolver',
        'Mageplaza_Osc/js/model/address/auto-complete',
        'Magento_Checkout/js/model/full-screen-loader',
        'Clkweb_PackageShops/js/model/shipping',
        'rjsResolver'
    ],
    function ($,
              _,
              ko,
              Component,
              quote,
              customer,
              setShippingInformationAction,
              getPaymentTotalInformation,
              stepNavigator,
              additionalValidators,
              checkoutData,
              selectBillingAddress,
              selectShippingAddress,
              addressConverter,
              shippingRateService,
              shippingService,
              oscDataResolver,
              addressAutoComplete,
              fullScreenLoader,
              psShipping,
              resolver) {
        'use strict';

        oscDataResolver.resolveDefaultShippingMethod();

        /** Set shipping methods to collection */
        shippingService.setShippingRates(window.checkoutConfig.shippingMethods);

        return Component.extend({
            defaults: {
                template: 'Mageplaza_Osc/container/shipping'
            },
            currentMethod: null,
            zipCode: ko.observable(),
            personPickup: ko.observable(''),
            packagePlacement: ko.observable(''),
            availableParcelShops: ko.observableArray([]),
            selectedParcelShop: ko.observable(),
            showZip: false,
            showPackagePlacement: false,
            selectedShippingCarrierCode: '',
            selectedShippingMethodCode: ko.computed(function () {
                    return quote.shippingMethod() ? quote.shippingMethod().method_code : null;
                }
            ),
            countryCode: ko.computed(function () {
                    return quote.shippingAddress() ? quote.shippingAddress().countryId : null;
                }
            ),
            initialize: function () {
                this._super();

                var self = this;

                stepNavigator.steps.removeAll();

                shippingRateService.estimateShippingMethod();
                additionalValidators.registerValidator(this);

                resolver(this.afterResolveDocument.bind(this));

                this.selectedShippingMethodCode.subscribe(function() {
                    var shippingRates = shippingService.getShippingRates()();

                    for (var key in shippingRates) {
                        if (shippingRates[key].method_code == self.selectedShippingMethodCode()) {
                            self.showZip = shippingRates[key].show_zip;
                            self.selectedShippingCarrierCode = shippingRates[key].agent_code;
                            self.showPackagePlacement = shippingRates[key].show_package_placement;
                        }
                    }

                    // if current shipping method has zip input available, then reset available shops and reset zip value
                    // to '' and then back to previous value, so ajax search gets called
                    if (self.showZip !== '') {
                        var oldZip = self.zipCode();

                        self.zipCode('');
                        self.availableParcelShops([]);
                        self.zipCode(oldZip);
                    }
                });

                this.zipCode.subscribe(function(value) {
                    var skipSearch = false;

                    if (self.selectedShippingCarrierCode == '') {

                        skipSearch = true;
                    } else if (self.countryCode() != 'NL') {
                        // remove space, as format for sweden has space, for example, "123 54"
                        if (self.countryCode() == 'SE' && value != undefined) {
                            value = value.replace(/\s/g, '');
                        }
                        value = parseInt(value, 10);
                        //  if is number
                        if (!isNaN(value)) {

                            if ((self.countryCode() == 'DK' || self.countryCode() == 'NO' ||
                                    self.countryCode() == 'BE'|| self.countryCode() == 'LU') && (value < 1000 || value > 9999)) {
                                skipSearch = true;
                            } else if ((self.countryCode() == 'SE' || self.countryCode() == 'FI' || self.countryCode() == 'DE') &&
                                (value < 10000 || value > 99999)) {
                                skipSearch = true;
                            }
                        } else {
                            skipSearch = true;
                        }
                    }

                    if (!skipSearch) {
                        fullScreenLoader.startLoader();
                        $.ajax({
                            url: window.authenticationPopup.baseUrl + 'clkwebpackageshops/index/index/',
                            dataType: 'json',
                            context: document.body,
                            data: {
                                'carrierCode': self.selectedShippingCarrierCode,
                                'zipCode': value,
                                'country': self.countryCode()
                            }
                        }).done(
                            function (response) {
                                if (response.parcelShops && response.parcelShops.length > 0) {
                                    self.availableParcelShops([]);
                                    for (var key in response.parcelShops) {
                                        if (response.parcelShops.hasOwnProperty(key)) {
                                            self.availableParcelShops.push(response.parcelShops[key]);
                                        }
                                    }
                                }
                                fullScreenLoader.stopLoader();
                            }
                        ).fail(
                            function () {
                                fullScreenLoader.stopLoader();
                            }
                        );
                    }
                });

                return this;
            },

            initObservable: function () {
                this._super();

                quote.shippingMethod.subscribe(function (oldValue) {
                    this.currentMethod = oldValue;
                }, this, 'beforeChange');

                quote.shippingMethod.subscribe(function (newValue) {
                    var isMethodChange = ($.type(this.currentMethod) !== 'object') ? true : this.currentMethod.method_code;
                    if ($.type(newValue) === 'object' && (isMethodChange !== newValue.method_code)) {
                        setShippingInformationAction();
                    } else if (shippingRateService.isAddressChange) {
                        shippingRateService.isAddressChange = false;
                        getPaymentTotalInformation();
                    }
                }, this);

                return this;
            },

            afterResolveDocument: function () {
                addressAutoComplete.register('shipping');
            },

            validate: function () {
                if (quote.isVirtual()) {
                    return true;
                }

                var shippingMethodValidationResult = true,
                    shippingAddressValidationResult = true,
                    loginFormSelector = 'form[data-role=email-with-possible-login]',
                    emailValidationResult = customer.isLoggedIn();

                if (!quote.shippingMethod()) {
                    this.errorValidationMessage('Please specify a shipping method.');

                    shippingMethodValidationResult = false;
                }

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }

                if (this.isFormInline) {
                    this.source.set('params.invalid', false);
                    this.source.trigger('shippingAddress.data.validate');

                    if (this.source.get('shippingAddress.custom_attributes')) {
                        this.source.trigger('shippingAddress.custom_attributes.data.validate');
                    }

                    if (this.source.get('params.invalid')) {
                        shippingAddressValidationResult = false;
                    }

                    this.saveShippingAddress();
                }

                if (!emailValidationResult) {
                    $(loginFormSelector + ' input[name=username]').focus();
                }

                var validationResult = true;
                var parcelShopId = parseInt(this.selectedParcelShop(), 10);
                var parcelShop = {};
                var parcelShopObject = {};
                var street = [];

                if (isNaN(parcelShopId)) {
                    parcelShopId = 0;
                }

                if (this.selectedShippingCarrierCode !== '' && validationResult) {
                    if (this.showZip !== '') {
                        if (parcelShopId == 0) {

                            this.errorValidationMessage($.mage.__('Please specify package shop.'));
                            validationResult = false;
                        } else if (this.personPickup().trim().length == 0) {

                            this.errorValidationMessage($.mage.__('Please enter name of the person who picks up the package.'));
                            validationResult = false;
                        } else {
                            psShipping.psShippingAddressUpdate(0);
                            this.packagePlacement('');

                            this.errorValidationMessage('');
                            var parcelShops = this.availableParcelShops();

                            for (var key in parcelShops) {
                                if (parcelShops[key].number == parcelShopId) {
                                    parcelShopObject = parcelShops[key];
                                    parcelShop = JSON.stringify(parcelShops[key]);
                                }
                            }

                            psShipping.shippingAddress(Object.create(quote.shippingAddress()));

                            psShipping.shippingAddress().firstname = this.personPickup().trim();
                            psShipping.shippingAddress().firstname = this.personPickup().trim();
                            psShipping.shippingAddress().lastname = null;
                            street.push(parcelShopObject.address);
                            street.push(parcelShopObject.address2);
                            psShipping.shippingAddress().street = street;
                            psShipping.shippingAddress().city = parcelShopObject.city;
                            psShipping.shippingAddress().countryId = parcelShopObject.country;
                            psShipping.shippingAddress().company = parcelShopObject.company_name;
                            psShipping.shippingAddress().telephone = null;
                            psShipping.shippingAddress().postcode = parcelShopObject.zipcode;

                            psShipping.psShippingAddressUpdate(1);
                        }
                    } else if (this.showPackagePlacement !== '') {
                        if (this.packagePlacement().trim().length == 0) {

                            this.errorValidationMessage($.mage.__('Please enter, where package should be placed.'));
                            validationResult = false;
                        } else {
                            this.errorValidationMessage('');
                        }
                    }
                } else {
                    psShipping.psShippingAddressUpdate(0);
                }

                if (this.showZip == '') {
                    this.personPickup('');
                }

                if (this.showPackagePlacement == '') {
                    this.packagePlacement('');
                }

                $.ajax({
                    url: window.authenticationPopup.baseUrl + 'clkwebpackageshops/index/saveshop/',
                    method: 'POST',
                    data: {
                        'parcelShop': parcelShop,
                        'personPickup': this.personPickup().trim(),
                        'packagePlacement': this.packagePlacement().trim(),
                        'carrierCode': this.selectedShippingCarrierCode
                    }
                });

                return shippingMethodValidationResult && shippingAddressValidationResult && emailValidationResult && validationResult;
            },
            saveShippingAddress: function () {
                var shippingAddress = quote.shippingAddress(),
                    addressData = addressConverter.formAddressDataToQuoteAddress(
                        this.source.get('shippingAddress')
                    );

                //Copy form data to quote shipping address object
                for (var field in addressData) {
                    if (addressData.hasOwnProperty(field) &&
                        shippingAddress.hasOwnProperty(field) &&
                        typeof addressData[field] != 'function' &&
                        _.isEqual(shippingAddress[field], addressData[field])
                    ) {
                        shippingAddress[field] = addressData[field];
                    } else if (typeof addressData[field] != 'function' && !_.isEqual(shippingAddress[field], addressData[field])) {
                        shippingAddress = addressData;
                        break;
                    }
                }

                if (customer.isLoggedIn()) {
                    shippingAddress.save_in_address_book = 1;
                }
                selectShippingAddress(shippingAddress);
            },

            saveNewAddress: function () {
                this.source.set('params.invalid', false);
                if (this.source.get('shippingAddress.custom_attributes')) {
                    this.source.trigger('shippingAddress.custom_attributes.data.validate');
                }

                if (!this.source.get('params.invalid')) {
                    this._super();
                }

                if (!this.source.get('params.invalid')) {
                    shippingRateService.isAddressChange = true;
                    shippingRateService.estimateShippingMethod();
                }
            },

            getAddressTemplate: function () {
                return 'Mageplaza_Osc/container/address/shipping-address';
            }
        });
    }
);
