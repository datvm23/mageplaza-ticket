define([
    'jquery',
    'Mageplaza_Osc/js/action/set-checkout-information',
    'Mageplaza_Osc/js/model/braintree-paypal',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Checkout/js/model/quote'
], function ($, setCheckoutInformationAction, braintreePaypalModel, additionalValidators, quote) {
    'use strict';
    return function (BraintreePaypalComponent) {
        return BraintreePaypalComponent.extend({
            /**
             * Set list of observable attributes
             * @returns {exports.initObservable}
             */
            initObservable: function () {
                var self = this;

                this._super();
                // for each component initialization need update property
                this.isReviewRequired = braintreePaypalModel.isReviewRequired;
                this.customerEmail = braintreePaypalModel.customerEmail;
                this.active = braintreePaypalModel.active;

                var x = setInterval(function () {
                    if ($('#paypal-container').length){
                        clearInterval(x);
                        self.initClientConfig();
                    }
                }, 200);

                return this;
            },

            /**
             * Get shipping address
             * @returns {Object}
             */
            getShippingAddress: function () {
                var address = quote.shippingAddress();

                if (!address) {
                    address = {};
                }

                if (!address.street) {
                    address.street = [];
                }

                if (address.postcode === null) {

                    return {};
                }

                return {
                    recipientName: address.firstname + ' ' + address.lastname,
                    streetAddress: address.street[0],
                    locality: address.city,
                    countryCodeAlpha2: address.countryId,
                    postalCode: address.postcode,
                    region: address.regionCode,
                    phone: address.telephone,
                    editable: this.isAllowOverrideShippingAddress()
                };
            },

            /**
             * Triggers when payment method change
             * @param {Boolean} isActive
             */
            onActiveChange: function (isActive) {
                if (!isActive) {
                    return;
                }

                var self = this;
                var x = setInterval(function () {
                    if ($('#paypal-container').length) {
                        clearInterval(x);
                        // need always re-init Braintree with PayPal configuration
                        self.reInitPayPal();
                    }
                }, 200);
            }
        })
    }
});