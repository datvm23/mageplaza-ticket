/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2017 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'Magento_Checkout/js/model/shipping-rate-service',
    'Magento_Checkout/js/model/shipping-service'
], function ($, shippingRateService, shippingService) {
    'use strict';
    return function (AmazonWidgetAddressComponent) {
        return AmazonWidgetAddressComponent.extend({
            /**
             * Get shipping address from Amazon API
             */
            getShippingAddressFromAmazon: function () {
                this._super();

                var onChangeAddress = setInterval(function () {
                    if (!shippingService.isLoading()) {
                        clearInterval(onChangeAddress);
                        shippingRateService.estimateShippingMethod();
                    }
                }, 200);

                return this;
            }
        })
    }
});