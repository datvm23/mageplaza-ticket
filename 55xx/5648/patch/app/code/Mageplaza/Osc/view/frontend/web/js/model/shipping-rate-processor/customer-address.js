/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'jquery',
        'Magento_Checkout/js/model/resource-url-manager',
        'Magento_Checkout/js/model/quote',
        'mage/storage',
        'Magento_Checkout/js/model/shipping-service',
        'Magento_Checkout/js/model/shipping-rate-registry',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/action/set-billing-address',
        'Magento_Checkout/js/action/set-shipping-information',
        'Magento_Checkout/js/action/select-shipping-method',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/view/billing-address'
    ],
    function (
        $,
        resourceUrlManager,
        quote,
        storage,
        shippingService,
        rateRegistry,
        errorProcessor,
        setBillingAddressAction,
        setShippingInformationAction,
        selecteShippingMethodAction,
        checkoutData,
        billingAddress
    ) {
        "use strict";

        return {
            getRates: function(address) {
                shippingService.isLoading(true);
                // var cache = rateRegistry.get(address.getKey());
                var cache = null;
                if (cache) {
                    shippingService.setShippingRates(cache);
                    shippingService.isLoading(false);
                } else {
                    storage.post(
                        resourceUrlManager.getUrlForEstimationShippingMethodsByAddressId(),
                        JSON.stringify({
                            addressId: address.customerAddressId
                        }),
                        false
                    ).done(
                        function(result) {
                            rateRegistry.set(address.getKey(), result);
                            shippingService.setShippingRates(result);

                            var isMethodSelected = true;
                            $.each(result, function(index, method) {
                                if (method == quote.shippingMethod()) {
                                    isMethodSelected = false;
                                }
                            });
                            if (isMethodSelected && result[0]) {
                                selecteShippingMethodAction(result[0]);
                                checkoutData.setSelectedShippingRate(result[0].carrier_code + '_' + result[0].method_code);
                            }

                            var interval = setInterval(function() {
                                if(quote.shippingAddress() && quote.billingAddress()) {
                                    clearInterval(interval);
                                    if (billingAddress().isAddressSameAsShipping()) {
                                        quote.billingAddress(quote.shippingAddress());
                                    }
                                    setShippingInformationAction().done(function () {
                                        setBillingAddressAction();
                                    });
                                }
                            }, 200);
                        }
                    ).fail(
                        function(response) {
                            shippingService.setShippingRates([]);
                            errorProcessor.process(response);
                        }
                    ).always(
                        function () {
                            shippingService.isLoading(false);
                        }
                    );
                }
            }
        };
    }
);
