<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2017 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Osc\Model\Plugin\Checkout;

/**
 * Class TotalsInformationManagement
 * @package Mageplaza\Osc\Model\Plugin\Checkout
 */

class TotalsInformationManagement
{
	/**
	 * @var \Magento\Quote\Api\CartRepositoryInterface
	 */
	protected $quoteRepository;

	/**
	 * @var \Magento\Quote\Api\CartTotalRepositoryInterface
	 */
	protected $cartTotalRepository;

	/**
	 * @var \Magento\Checkout\Model\Type\Onepage
	 */
	protected $onePage;

	/**
	 * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
	 * @param \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalRepository
	 * @param \Magento\Checkout\Model\Type\Onepage $onePage
	 */
	public function __construct(
		\Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
		\Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalRepository,
		\Magento\Checkout\Model\Type\Onepage $onePage
	)
	{
		$this->quoteRepository   = $quoteRepository;
		$this->cartTotalRepository = $cartTotalRepository;
		$this->onePage = $onePage;
	}

	/**
	 * @param $cartId
	 * @param \Magento\Checkout\Model\TotalsInformationManagement $subject
	 * @param \Magento\Checkout\Api\Data\TotalsInformationInterface $addressInformation
	 * @return mixed
	 */
	public function aroundCalculate(
		\Magento\Checkout\Model\TotalsInformationManagement $subject,
		\Closure $proceed,
		$cartId,
		\Magento\Checkout\Api\Data\TotalsInformationInterface $addressInformation
	) {
		/* @var \Magento\Quote\Model\Quote $quote */
		$proceed($cartId, $addressInformation);

		$quote = $this->quoteRepository->get($cartId);
		if (!$quote->isVirtual()) {
			$methodCode = $addressInformation->getShippingCarrierCode() . '_' . $addressInformation->getShippingMethodCode();
			$this->onePage->saveShippingMethod($methodCode);
			$this->quoteRepository->save($quote);
		}

		return $this->cartTotalRepository->get($cartId);
	}
}