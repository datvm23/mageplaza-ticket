<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
namespace Mageplaza\Osc\Model\Plugin\Quote;

class ShippingMethodManagement
{
	/**
	 * Lists applicable shipping methods for a specified quote.
	 *
	 * @param \Magento\Quote\Model\ShippingMethodManagement $subject
	 * @param \Magento\Quote\Api\Data\ShippingMethodInterface[] $result
	 * @return mixed
	 */
	public function afterGetList(\Magento\Quote\Model\ShippingMethodManagement $subject, $result)
	{
		$output = [];
		foreach ($result as $item) {
			if ($item->getAvailable() && $item->getCarrierCode() == 'freeshipping') {
				$output[] = $item;
				break;
			}
		}
		return $output ?: $result;
	}

	/**
	 * Get list of available shipping methods
	 *
	 * @param \Magento\Quote\Model\ShippingMethodManagement $subject
	 * @param \Magento\Quote\Api\Data\ShippingMethodInterface[] $result
	 * @return mixed
	 */
	public function afterEstimateByExtendedAddress(\Magento\Quote\Model\ShippingMethodManagement $subject, $result)
	{
		$output = [];
		foreach ($result as $item) {
			if ($item->getAvailable() && $item->getCarrierCode() == 'freeshipping') {
				$output[] = $item;
				break;
			}
		}
		return $output ?: $result;
	}
}
