/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/view/payment',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Mageplaza_Osc/js/model/checkout-data-resolver',
        'Mageplaza_Osc/js/model/payment-service',
        'rjsResolver',
        'Mageplaza_Osc/js/model/osc-data',
        'mage/translate'
    ],
    function (ko,
              $,
              Component,
              quote,
              stepNavigator,
              additionalValidators,
              oscDataResolver,
              oscPaymentService,
              resolver,
              oscData) {
        'use strict';

        oscDataResolver.resolveDefaultPaymentMethod();

        return Component.extend({
            defaults: {
                template: 'Mageplaza_Osc/container/payment'
            },
            isLoading: oscPaymentService.isLoading,
            errorValidationMessage: ko.observable(false),

            initialize: function () {
                var self = this;

                this._super();

                stepNavigator.steps.removeAll();

                additionalValidators.registerValidator(this);

                quote.paymentMethod.subscribe(function () {
                    self.errorValidationMessage(false);
                });

                resolver(this.initEvent.bind(this));

                return this;
            },

            initEvent: function () {
                $('#zs_usaepay_cc_number, #zs_usaepay_cc_cid, #zs_usaepay_expiration_yr, #zs_usaepay_expiration').on('change', function () {
                    oscData.customEvent(false);
                    oscData.customEvent(true);
                });
            },

            validate: function () {
                if (!quote.paymentMethod()) {
                    this.errorValidationMessage($.mage.__('Please specify a payment method.'));

                    return false;
                }

                return true;
            }
        });
    }
);
