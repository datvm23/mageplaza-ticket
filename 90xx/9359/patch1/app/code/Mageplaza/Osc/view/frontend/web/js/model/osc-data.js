/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'Magento_Customer/js/customer-data',
    'ko',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/payment/additional-validators'
], function ($, storage,ko, quote, additionalValidators) {
    'use strict';

    var cacheKey = 'osc-data';

    var getData = function () {
        return storage.get(cacheKey)();
    };

    var saveData = function (checkoutData) {
        storage.set(cacheKey, checkoutData);
    };

    var checkFields = function () {
        var paymentFields = ['zs_usaepay_expiration', 'zs_usaepay_expiration_yr', 'zs_usaepay_cc_cid', 'zs_usaepay_cc_number'],
            validate,
            result = true;

        $.each(paymentFields, function (index, field) {
            validate = $('#' + field);
            if (typeof validate === undefined || validate.length <= 0 || !validate.val()) {
                result = false;
                return false;
            }
        });

        return result;
    };

    var customEvent = ko.observable(true);

    return {
        customEvent: customEvent,
        customValidate: ko.computed(function () {
            var payment = quote.paymentMethod();

            if (payment != null && payment.method == 'zs_usaepay' && customEvent()) {
                return checkFields();
            }

            return true;
        }),

        checkFields: function () {
            return checkFields();
        },

        setData: function (key, data) {
            var obj = getData();
            obj[key] = data;
            saveData(obj);
        },

        getData: function (key) {
            if (typeof key === 'undefined') {
                return getData();
            }

            return getData()[key];
        }
    }
});
