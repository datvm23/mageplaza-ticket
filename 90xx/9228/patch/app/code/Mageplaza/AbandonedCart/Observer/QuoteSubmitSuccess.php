<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_AbandonedCart
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\AbandonedCart\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Mageplaza\AbandonedCart\Model\LogsFactory;
use Mageplaza\AbandonedCart\Model\ResourceModel\Logs\CollectionFactory as LogsCollectionFactory;

/**
 * Class QuoteSubmitSuccess
 * @package Mageplaza\AbandonedCart\Observer
 */
class QuoteSubmitSuccess implements ObserverInterface
{

    /**
     * @var LogsFactory
     */
    protected $logsFactory;

    /**
     * @var LogsCollectionFactory
     */
    protected $logsCollection;

    /**
     * @param LogsFactory $logsFactory
     * @param LogsCollectionFactory $logsCollection
     */
    public function __construct(
        LogsFactory $logsFactory,
        LogsCollectionFactory $logsCollection
    )
    {
        $this->logsFactory    = $logsFactory;
        $this->logsCollection = $logsCollection;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(Observer $observer)
    {
        /** @type \Magento\Quote\Model\Quote $quote $quote */
        $quote = $observer->getEvent()->getQuote();
        /** @type \Magento\Sales\Api\Data\OrderInterface $order */
        $order = $observer->getEvent()->getOrder();

        $collection = $this->logsCollection->create()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('recovery', 1);

        if (count($collection)) {
            $logs = $this->logsFactory->create();
            $logs->updateRecovery($quote->getId(), 2);
            $logs->updateOrderId($quote->getId(), $order->getIncrementId());
        }
    }
}
