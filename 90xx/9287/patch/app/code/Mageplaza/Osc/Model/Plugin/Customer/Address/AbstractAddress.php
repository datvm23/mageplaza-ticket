<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Osc\Model\Plugin\Customer\Address;

use Mageplaza\Osc\Helper\Data as OscHelper;

/**
 * Class AbstractAddress
 * @package Mageplaza\Osc\Model\Plugin\Customer\Address
 */
class AbstractAddress
{
    /**
     * @var OscHelper
     */
    private $_oscHelper;

    /**
     * AbstractAddress constructor.
     * @param OscHelper $oscHelper
     */
    public function __construct(
        OscHelper $oscHelper
    )
    {
        $this->_oscHelper = $oscHelper;
    }

    /**
     * @param \Magento\Customer\Model\Address\AbstractAddress $subject
     * @param $errors
     * @return mixed
     */
    public function afterValidate(\Magento\Customer\Model\Address\AbstractAddress $subject, $errors)
    {
        if ($errors === true) {
            return true;
        }

        $result = false;

        $fieldPosition = $this->_oscHelper->getAddressHelper()->getAddressFieldPosition();

        foreach ($errors as $item) {
            if (strpos($item, __(' is a required field.')->getText()) !== false) {
                $errorField = explode(' ', $item);
                if (!isset($fieldPosition[$errorField[0]])) {
                    $result = true;
                    break;
                }
            }
        }

        return $result ?: $errors;
    }
}
