<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Blog
 * @copyright   Copyright (c) 2018 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Blog\Model;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;
use Mageplaza\Blog\Helper\Data;
use Mageplaza\Blog\Helper\Image;

/**
 * Class Sitemap
 * @package Mageplaza\Blog\Model
 */
class Sitemap extends \Magento\Sitemap\Model\Sitemap
{
    /**
     * @var \Mageplaza\Blog\Helper\Data
     */
    protected $blogDataHelper;

    /**
     * @var \Mageplaza\Blog\Helper\Image
     */
    protected $imageHelper;

    /**
     * @var mixed
     */
    protected $router;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->blogDataHelper = ObjectManager::getInstance()->get(Data::class);
        $this->imageHelper = ObjectManager::getInstance()->get(Image::class);;
        $this->router = $this->blogDataHelper->getBlogConfig('general/url_prefix');
    }

    /**
     * @param $storeId
     * @return array
     */
    public function getBlogPostsSiteMapCollection($storeId)
    {
        $urlSuffix = $this->blogDataHelper->getUrlSuffix();
        $postCollection = $this->blogDataHelper->postFactory->create()->getCollection();
        $postCollection = $this->blogDataHelper->addStoreFilter($postCollection, $storeId);
        $postSiteMapCollection = [];
        if (!$this->router) {
            $this->router = 'blog';
        }
        /** @var \Mageplaza\Blog\Model\Post $item */
        foreach ($postCollection as $item) {
            if (!is_null($item->getEnabled())) {
                $images = null;
                $imagesCollection = null;
                if ($item->getImage()) :
                    $imageFile = $this->imageHelper->getMediaPath($item->getImage(), Image::TEMPLATE_MEDIA_TYPE_POST);

                    $imagesCollection[] = new DataObject([
                            'url' => $this->imageHelper->getMediaUrl($imageFile),
                            'caption' => null,
                        ]
                    );
                    $images = new DataObject(['collection' => $imagesCollection]);
                endif;
                $postSiteMapCollection[$item->getId()] = new DataObject([
                    'id' => $item->getId(),
                    'url' => $this->router . '/post/' . $item->getUrlKey() . $urlSuffix,
                    'images' => $images,
                    'updated_at' => $item->getUpdatedAt(),
                ]);
            }
        }

        return $postSiteMapCollection;
    }

    /**
     * @param $storeId
     * @return array
     */
    public function getBlogCategoriesSiteMapCollection($storeId)
    {
        $urlSuffix = $this->blogDataHelper->getUrlSuffix();
        $categoryCollection = $this->blogDataHelper->categoryFactory->create()->getCollection();
        $categoryCollection = $this->blogDataHelper->addStoreFilter($categoryCollection, $storeId);
        $categorySiteMapCollection = [];
        /** @var \Mageplaza\Blog\Model\Category $item */
        foreach ($categoryCollection as $item) {
            if (!is_null($item->getEnabled())) {
                $categorySiteMapCollection[$item->getId()] = new DataObject([
                    'id' => $item->getId(),
                    'url' => $this->router . '/category/' . $item->getUrlKey() . $urlSuffix,
                    'updated_at' => $item->getUpdatedAt(),
                ]);
            }
        }

        return $categorySiteMapCollection;
    }

    /**
     * @param $storeId
     * @return array
     */
    public function getBlogTagsSiteMapCollection($storeId)
    {
        $urlSuffix = $this->blogDataHelper->getUrlSuffix();
        $tagCollection = $this->blogDataHelper->tagFactory->create()->getCollection();
        $tagCollection = $this->blogDataHelper->addStoreFilter($tagCollection, $storeId);
        $tagSiteMapCollection = [];
        /** @var \Mageplaza\Blog\Model\Tag $item */
        foreach ($tagCollection as $item) {
            if (!is_null($item->getEnabled())) {
                $tagSiteMapCollection[$item->getId()] = new DataObject([
                    'id' => $item->getId(),
                    'url' => $this->router . '/tag/' . $item->getUrlKey() . $urlSuffix,
                    'updated_at' => $item->getUpdatedAt(),
                ]);
            }
        }

        return $tagSiteMapCollection;
    }

    /**
     * @param $storeId
     * @return array
     */
    public function getBlogTopicsSiteMapCollection($storeId)
    {
        $urlSuffix = $this->blogDataHelper->getUrlSuffix();
        $topicCollection = $this->blogDataHelper->topicFactory->create()->getCollection();
        $topicCollection = $this->blogDataHelper->addStoreFilter($topicCollection, $storeId);
        $topicSiteMapCollection = [];
        /** @var \Mageplaza\Blog\Model\Topic $item */
        foreach ($topicCollection as $item) {
            if (!is_null($item->getEnabled())) {
                $topicSiteMapCollection[$item->getId()] = new DataObject([
                    'id' => $item->getId(),
                    'url' => $this->router . '/topic/' . $item->getUrlKey() . $urlSuffix,
                    'updated_at' => $item->getUpdatedAt(),
                ]);
            }
        }

        return $topicSiteMapCollection;
    }

    /**
     * @inheritdoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _initSitemapItems()
    {
        $storeId = $this->getStoreId();
        $this->_sitemapItems[] = new DataObject([
                'collection' => $this->getBlogPostsSiteMapCollection($storeId),
            ]
        );
        $this->_sitemapItems[] = new DataObject([
                'collection' => $this->getBlogCategoriesSiteMapCollection($storeId),
            ]
        );
        $this->_sitemapItems[] = new DataObject([
                'collection' => $this->getBlogTagsSiteMapCollection($storeId),
            ]
        );
        $this->_sitemapItems[] = new DataObject([
                'collection' => $this->getBlogTopicsSiteMapCollection($storeId),
            ]
        );

        parent::_initSitemapItems(); // TODO: Change the autogenerated stub
    }
}
