Fix problem with shipping method in cart page and osc page
Fix order total summary not updated when:
- customer/new address is changed
- shipping method is same but price is changed according to addresses