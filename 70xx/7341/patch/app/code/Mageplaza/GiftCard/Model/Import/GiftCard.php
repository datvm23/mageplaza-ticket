<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_GiftCard
 * @copyright   Copyright (c) 2017 Mageplaza (https://www.mageplaza.com/)
 * @license     http://mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\GiftCard\Model\Import;

use Magento\Backend\Model\Auth;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Stdlib\StringUtils;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\AbstractEntity;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\ImportFactory;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Mageplaza\GiftCard\Helper\Data;
use Mageplaza\GiftCard\Model\GiftCard\Status;
use Mageplaza\GiftCard\Model\Import\GiftCard\RowValidatorInterface as ValidatorInterface;

/**
 * Class GiftCard
 * @package Mageplaza\GiftCard\Model\Import
 */
class GiftCard extends AbstractEntity
{
    /**
     * columns
     */
    const COL_CODE = 'code';
    const COL_BALANCE = 'balance';
    const COL_STATUS = 'status';
    const COL_CAN_REDEEM = 'can_redeem';
    const COL_STORE_ID = 'store_id';
    const COL_TEMPLATE_ID = 'template_id';
    const COL_POOL_ID = 'pool_id';
    const COL_EXPIRED_AT = 'expired_at';

    /**
     * Gift Card table
     */
    const TABLE_ENTITY = 'mageplaza_giftcard';

    /** @inheritdoc */
    protected $masterAttributeCode = 'code';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = [
        ValidatorInterface::ERROR_CODE_IS_EMPTY    => 'Code is empty',
        ValidatorInterface::ERROR_DUPLICATE_CODE   => 'Gift Code is duplicated',
        ValidatorInterface::ERROR_INVALID_POOL     => 'Pool in\'t exist',
        ValidatorInterface::ERROR_INVALID_TEMPLATE => 'Template in\'t exist',
        ValidatorInterface::ERROR_INVALID_WEBSITE  => 'Website in\'t exist',
        ValidatorInterface::ERROR_INVALID_STATUS   => 'Status not exist'
    ];

    /**
     * Permanent entity columns.
     *
     * @var string[]
     */
    protected $_permanentAttributes = [self::COL_CODE];

    /** @inheritdoc */
    protected $_availableBehaviors = [
        Import::BEHAVIOR_APPEND,
        Import::BEHAVIOR_REPLACE,
        Import::BEHAVIOR_DELETE
    ];

    /**
     * If we should check column names
     *
     * @var bool
     */
    protected $needColumnCheck = true;

    /**
     * Valid column names
     *
     * @array
     */
    protected $validColumnNames = [
        self::COL_CODE,
        self::COL_BALANCE,
        self::COL_CAN_REDEEM,
        self::COL_STATUS,
        self::COL_STORE_ID,
        self::COL_TEMPLATE_ID,
        self::COL_POOL_ID,
        self::COL_EXPIRED_AT
    ];

    /**
     * Need to log in import history
     *
     * @var bool
     */
    protected $logInHistory = true;

    /**
     * @var \Magento\Backend\Model\Auth
     */
    protected $_auth;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * Constructor
     *
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\ImportExport\Model\ImportFactory $importFactory
     * @param \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface $errorAggregator
     * @param \Magento\Backend\Model\Auth $auth
     * @param array $data
     */
    public function __construct(
        StringUtils $string,
        ScopeConfigInterface $scopeConfig,
        ImportFactory $importFactory,
        Helper $resourceHelper,
        ResourceConnection $resource,
        ProcessingErrorAggregatorInterface $errorAggregator,
        Auth $auth,
        array $data = []
    )
    {
        $this->_auth = $auth;
        $this->_resource = $resource;

        parent::__construct($string, $scopeConfig, $importFactory, $resourceHelper, $resource, $errorAggregator, $data);
    }

    /**
     * Entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return 'gift_card';
    }

    /**
     * Row validation.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum)
    {
        $code = false;

        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;
        // BEHAVIOR_DELETE use specific validation logic
        if (Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            if (!isset($rowData[self::COL_CODE])) {
                $this->addRowError(ValidatorInterface::ERROR_CODE_IS_EMPTY, $rowNum);

                return false;
            }

            return true;
        }
        if (isset($rowData[self::COL_CODE])) {
            $code = $rowData[self::COL_CODE];
        }
        if (false === $code) {
            $this->addRowError(ValidatorInterface::ERROR_CODE_IS_EMPTY, $rowNum);
        }

        if (isset($rowData[self::COL_STATUS]) && !in_array($rowData[self::COL_STATUS], Status::getStatus())) {
            $this->addRowError(ValidatorInterface::ERROR_INVALID_STATUS, $rowNum);
        }

        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }


    /**
     * Create Gift card data from raw data.
     *
     * @throws \Exception
     * @return bool Result of operation.
     */
    protected function _importData()
    {
        if (Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            $this->deleteEntity();
        } elseif (Import::BEHAVIOR_REPLACE == $this->getBehavior()) {
            $this->replaceEntity();
        } elseif (Import::BEHAVIOR_APPEND == $this->getBehavior()) {
            $this->saveEntity();
        }

        return true;
    }

    /**
     * Save gift card code
     *
     * @return $this
     */
    public function saveEntity()
    {
        $this->saveAndReplaceEntity();

        return $this;
    }

    /**
     * Replace newsletter subscriber
     *
     * @return $this
     */
    public function replaceEntity()
    {
        $this->saveAndReplaceEntity();

        return $this;
    }

    /**
     * Deletes gift card from raw data.
     *
     * @return $this
     */
    public function deleteEntity()
    {
        $listCode = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);
                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    $rowCode    = $rowData[self::COL_CODE];
                    $listCode[] = $rowCode;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }
        if ($listCode) {
            $this->deleteEntityFinish(array_unique($listCode), self::TABLE_ENTITY);
        }

        return $this;
    }

    /**
     * Save and replace gift card
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $listCode = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->addRowError(ValidatorInterface::ERROR_CODE_IS_EMPTY, $rowNum);
                    continue;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }

                $rowCode                = $rowData[self::COL_CODE];
                $listCode[]             = $rowCode;
                $entityList[$rowCode][] = [
                    self::COL_CODE        => $rowData[self::COL_CODE],
                    self::COL_BALANCE     => $rowData[self::COL_BALANCE],
                    self::COL_STATUS      => $rowData[self::COL_STATUS],
                    self::COL_CAN_REDEEM  => $rowData[self::COL_CAN_REDEEM],
                    self::COL_STORE_ID    => $rowData[self::COL_STORE_ID],
                    self::COL_TEMPLATE_ID => $rowData[self::COL_TEMPLATE_ID],
                    self::COL_POOL_ID     => $rowData[self::COL_POOL_ID],
                    self::COL_EXPIRED_AT  => $rowData[self::COL_EXPIRED_AT],
                    'extra_content'       => Data::jsonEncode(['auth' => $this->_auth->getUser()->getName()])
                ];
            }
            if (Import::BEHAVIOR_REPLACE == $behavior) {
                if ($listCode) {
                    if ($this->deleteEntityFinish(array_unique($listCode), self::TABLE_ENTITY)) {
                        $this->saveEntityFinish($entityList, self::TABLE_ENTITY);
                    }
                }
            } elseif (Import::BEHAVIOR_APPEND == $behavior) {
                $this->saveEntityFinish($entityList, self::TABLE_ENTITY);
            }
        }

        return $this;
    }

    /**
     * Save gift card
     *
     * @param array $entityData
     * @param string $table
     * @return $this
     */
    protected function saveEntityFinish(array $entityData, $table)
    {
        if ($entityData) {
            $tableName = $this->_resource->getTableName($table);
            $entityIn  = [];
            foreach ($entityData as $id => $entityRows) {
                foreach ($entityRows as $row) {
                    $entityIn[] = $row;
                }
            }

            if ($entityIn) {
                $this->_connection->insertOnDuplicate($tableName, $entityIn, [
                    self::COL_CODE,
                    self::COL_BALANCE,
                    self::COL_STATUS,
                    self::COL_CAN_REDEEM,
                    self::COL_STORE_ID,
                    self::COL_TEMPLATE_ID,
                    self::COL_POOL_ID,
                    self::COL_EXPIRED_AT,
                    'extra_content'
                ]);
            }
        }

        return $this;
    }

    /**
     * Deletes Gift Card code.
     *
     * @param array $listCode
     * @param string $table
     * @return boolean
     */
    protected function deleteEntityFinish(array $listCode, $table)
    {
        if ($table && $listCode) {
            try {
                $this->countItemsDeleted += $this->_connection->delete(
                    $this->_resource->getTableName($table),
                    $this->_connection->quoteInto('code IN (?)', $listCode)
                );

                return true;
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }
}
