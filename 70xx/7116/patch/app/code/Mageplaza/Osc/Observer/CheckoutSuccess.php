<?php

namespace Mageplaza\Osc\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class CheckoutSuccess implements ObserverInterface
{
    public function execute(EventObserver $observer)
    {
        $objectManager              = \Magento\Framework\App\ObjectManager::getInstance();
        $onePage                    = $objectManager->create('Magento\Checkout\Model\Type\Onepage');
        $session                    = $onePage->getCheckout();
        if ($session->getOscRegister()) {
            $quote                      = $session->getQuote();
            $emailNotificationInterface = $objectManager->create('Magento\Customer\Model\EmailNotificationInterface');
            $emailNotificationInterface->newAccount($quote->getCustomer(), 'registered', '', $quote->getCustomer()->getStoreId());
        }
    }
}