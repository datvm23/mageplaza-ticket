/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/set-shipping-information',
        'Mageplaza_Osc/js/model/shipping-rate-processor/new-address',
        'Mageplaza_Osc/js/model/shipping-rate-processor/customer-address'
    ],
    function (quote, setShippingInformationAction, defaultProcessor, customerAddressProcessor) {
        'use strict';

        var processors = [];

        processors.default = defaultProcessor;
        processors['customer-address'] = customerAddressProcessor;

        return {
            isAddressChange: false,
            registerProcessor: function (type, processor) {
                processors[type] = processor;
            },
            estimateShippingMethod: function () {
                var quoteInterval = setInterval(function() {
                    if(quote.shippingAddress() && quote.billingAddress()) {
                        clearInterval(quoteInterval);
                        if (quote.shippingMethod()) {
                            setShippingInformationAction().always(function () {
                                var type = quote.shippingAddress().getType();

                                if (processors[type]) {
                                    processors[type].getRates(quote.shippingAddress());
                                } else {
                                    processors.default.getRates(quote.shippingAddress());
                                }
                            });
                        } else {
                            var type = quote.shippingAddress().getType();

                            if (processors[type]) {
                                processors[type].getRates(quote.shippingAddress());
                            } else {
                                processors.default.getRates(quote.shippingAddress());
                            }
                        }
                    }
                }, 200);
            }
        }
    }
);
