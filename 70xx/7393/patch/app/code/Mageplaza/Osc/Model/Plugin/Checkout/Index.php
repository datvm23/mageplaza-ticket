<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2017-2018 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Osc\Model\Plugin\Checkout;

/**
 * Class Index
 * @package Mageplaza\Osc\Model\Plugin\Checkout
 */
class Index
{
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $_resultRedirectFactory;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $_coreSession;

    /**
     * Index constructor.
     * @param \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
     * @param \Magento\Backend\Model\Session $coreSession
     */
    public function __construct(
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory,
        \Magento\Backend\Model\Session $coreSession
    )
    {
        $this->_resultRedirectFactory = $redirectFactory;
        $this->_coreSession = $coreSession;
    }

    /**
     * @param \Closure $proceed
     * @return mixed
     */
    public function aroundExecute(
        \Magento\Checkout\Controller\Cart\Index $subject,
        \Closure $proceed
    )
    {
        if ($this->_coreSession->getRedirectUrl() == 'onestepcheckout') {
            $this->_coreSession->setRedirectUrl('');
            return $this->_resultRedirectFactory->create()->setPath('onestepcheckout');
        }
        return $proceed();
    }
}
